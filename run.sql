/*
Navicat MySQL Data Transfer

Source Server         : database
Source Server Version : 50505
Source Host           : localhost:3306
Source Database       : run

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2020-02-08 17:31:44
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `tbl_admin`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_admin`;
CREATE TABLE `tbl_admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `full_name` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `file_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_admin
-- ----------------------------
INSERT INTO `tbl_admin` VALUES ('1', 'admin', 'admin', 'e10adc3949ba59abbe56e057f20f883e', 'avatar-s-11.jpg');

-- ----------------------------
-- Table structure for `tbl_age`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_age`;
CREATE TABLE `tbl_age` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `age_title` varchar(255) DEFAULT NULL,
  `id_event` int(11) DEFAULT NULL,
  `create_at` datetime DEFAULT NULL,
  `update_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_age
-- ----------------------------
INSERT INTO `tbl_age` VALUES ('1', 'รุ่นอายุต่ำกว่า 19 ปี', '1', '2020-02-08 07:55:21', null);
INSERT INTO `tbl_age` VALUES ('2', 'รุ่นอายุต่ำกว่า 20 - 30 ปี', '1', '2020-02-08 07:55:21', null);

-- ----------------------------
-- Table structure for `tbl_blog`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_blog`;
CREATE TABLE `tbl_blog` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT '',
  `details` text DEFAULT NULL,
  `file_name` varchar(255) DEFAULT NULL,
  `type` varchar(100) DEFAULT NULL COMMENT '1//บทความ //2//ข่าวสาร',
  `create_at` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_blog
-- ----------------------------
INSERT INTO `tbl_blog` VALUES ('2', 'RUNgather Zodiac Series Challenge 2020', 'TEST', 'blog-1581145944.jpg', '2', '2020-02-08 08:12:24');

-- ----------------------------
-- Table structure for `tbl_contact`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_contact`;
CREATE TABLE `tbl_contact` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(100) DEFAULT NULL,
  `last_name` varchar(100) DEFAULT NULL,
  `tel` varchar(50) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `detail` text DEFAULT NULL,
  `create_at` datetime DEFAULT NULL,
  `update_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_contact
-- ----------------------------
INSERT INTO `tbl_contact` VALUES ('1', 'Nattaphon', 'Kiattikul', '0925623256', 'jame0925623256@gmail.com', 'ทดสอบอย่างมีระบบ', '2020-01-17 04:07:21', null);
INSERT INTO `tbl_contact` VALUES ('2', 'ทดสอบ', 'มีระบบบ', '0123456789', 'test@gmail.com', '123456 test', '2020-02-03 04:04:15', null);
INSERT INTO `tbl_contact` VALUES ('3', 'wipadsaya', 'jitra', '0968138751', 'qweqwr@gmail.com', 'ทดสอบบ', '2020-02-08 08:13:32', null);

-- ----------------------------
-- Table structure for `tbl_event`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_event`;
CREATE TABLE `tbl_event` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `member_id` int(11) DEFAULT NULL,
  `name_event` varchar(255) DEFAULT '',
  `details_event` text DEFAULT NULL,
  `type_event` int(11) DEFAULT NULL,
  `account_number` varchar(255) DEFAULT '',
  `bank` varchar(255) DEFAULT '',
  `size_s` varchar(20) DEFAULT '',
  `size_m` varchar(20) DEFAULT NULL,
  `size_l` varchar(20) DEFAULT NULL,
  `size_xl` varchar(20) DEFAULT NULL,
  `size_oversize` varchar(20) DEFAULT NULL,
  `file_cover` varchar(255) DEFAULT '',
  `file_shirt` varchar(255) DEFAULT NULL,
  `time_out` date DEFAULT NULL,
  `status` int(11) DEFAULT 0 COMMENT '0 กรุณาชำระเงิน 1 กำลังดำเนินงาน 2 เสร็จสิ้น 3 ล้มเหลว',
  `create_at` datetime DEFAULT NULL,
  `update_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_event
-- ----------------------------
INSERT INTO `tbl_event` VALUES ('1', '3', 'RUNgather Zodiac Series Challenge 2020', '<p>TEST</p>', '1', '4567891235', 'ธนาคารกสิกรไทย', '38', '40', '42', '44', '48', 'event-1581144921.jpg', 'shirt-1581144921.jpg', '2020-02-20', '2', '2020-02-08 07:55:21', null);

-- ----------------------------
-- Table structure for `tbl_marathon`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_marathon`;
CREATE TABLE `tbl_marathon` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_event` int(11) DEFAULT NULL,
  `name_marathon` varchar(255) DEFAULT '',
  `length` varchar(100) DEFAULT NULL,
  `price` varchar(100) DEFAULT NULL,
  `create_at` datetime DEFAULT NULL,
  `update_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_marathon
-- ----------------------------
INSERT INTO `tbl_marathon` VALUES ('1', '1', 'fun marathons', '5', '200', '2020-02-08 07:55:21', null);
INSERT INTO `tbl_marathon` VALUES ('2', '1', 'mini marathons', '14', '500', '2020-02-08 07:55:21', null);

-- ----------------------------
-- Table structure for `tbl_member`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_member`;
CREATE TABLE `tbl_member` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(255) DEFAULT '',
  `email` varchar(255) DEFAULT '',
  `password` varchar(255) DEFAULT NULL,
  `file_name` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `gender` varchar(100) DEFAULT NULL,
  `create_at` datetime DEFAULT NULL,
  `update_at` datetime DEFAULT NULL,
  `tel` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_member
-- ----------------------------
INSERT INTO `tbl_member` VALUES ('1', 'ทดสอบ', 'test@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', 'avatar-s-11.jpg', 'อย่างมีระบบ', 'หญิง', '2020-01-15 13:21:06', '2020-02-08 07:05:57', '0879879871');
INSERT INTO `tbl_member` VALUES ('2', 'ทดสอบพ่อค้า', 'test1@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', null, 'พ่อค้า', 'ชาย', '2020-02-03 04:05:45', null, '0123456789');
INSERT INTO `tbl_member` VALUES ('3', 'wipadsaya', 'beer_05199@hotmail.co.th', '0980ab8a3f895305444c974af4a22a0e', null, 'jitra', 'หญิง', '2020-02-08 07:49:05', '2020-02-08 07:49:58', '0873014403');
INSERT INTO `tbl_member` VALUES ('4', 'test12', 'test@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', null, '3456', 'ชาย', '2020-02-08 09:13:47', null, '0848449988');

-- ----------------------------
-- Table structure for `tbl_order`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_order`;
CREATE TABLE `tbl_order` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `member_id` int(11) DEFAULT NULL,
  `event_id` int(11) DEFAULT NULL,
  `reference` varchar(100) DEFAULT '',
  `time` time DEFAULT NULL,
  `create_at` datetime DEFAULT NULL,
  `update_at` datetime DEFAULT NULL,
  `date` date DEFAULT NULL,
  `note` varchar(255) DEFAULT NULL,
  `file_name` varchar(255) DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_order
-- ----------------------------
INSERT INTO `tbl_order` VALUES ('1', '3', '1', '456789789312', '14:00:00', '2020-02-08 08:00:28', null, '2020-02-08', 'TEST', 'payment-1581145228.jpg');

-- ----------------------------
-- Table structure for `tbl_order_detail`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_order_detail`;
CREATE TABLE `tbl_order_detail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_order` int(11) NOT NULL,
  `id_restaurant` int(11) NOT NULL,
  `name_item` varchar(255) DEFAULT NULL,
  `qty` int(11) NOT NULL,
  `price_item` varchar(255) DEFAULT NULL,
  `sumtotal` varchar(255) DEFAULT NULL,
  `file_name` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_order_detail
-- ----------------------------

-- ----------------------------
-- Table structure for `tbl_register_run`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_register_run`;
CREATE TABLE `tbl_register_run` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_event` int(11) NOT NULL,
  `full_name` varchar(255) DEFAULT NULL,
  `age` int(11) DEFAULT NULL,
  `id_card` varchar(255) DEFAULT NULL,
  `tel` varchar(255) DEFAULT NULL,
  `file_name` varchar(255) DEFAULT NULL,
  `status` int(11) DEFAULT 0,
  `create_at` datetime DEFAULT NULL,
  `marathon_id` tinyint(1) NOT NULL,
  `age_rank_id` int(11) NOT NULL,
  `size` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_register_run
-- ----------------------------
INSERT INTO `tbl_register_run` VALUES ('1', '1', 'ศุภมาส', '22', '1501001096791', '0882715226', 'payment-1581145581.jpg', '2', '2020-02-08 08:06:21', '2', '2', 'S 38');

-- ----------------------------
-- Table structure for `tbl_type`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_type`;
CREATE TABLE `tbl_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `price` varchar(255) DEFAULT NULL,
  `people` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_type
-- ----------------------------
INSERT INTO `tbl_type` VALUES ('1', 'แพ๊คเกจขนาด S รับคนสมัครได้สูงสุด', '800', '500');
INSERT INTO `tbl_type` VALUES ('3', 'แพ๊คเกจขนาด M รับคนสมัครได้สูงสุด', '1500', '1000');
INSERT INTO `tbl_type` VALUES ('4', 'แพ๊คเกจขนาด L รับคนสมัครได้สูงสุด', '2000', '2000');
