<style>
	.map {
		height: 250px;
		width: 100%
	}
</style>


<!-- BEGIN: Content-->
<div class="app-content content">
	<div class="content-overlay"></div>
	<div class="header-navbar-shadow"></div>
	<div class="content-wrapper">
		<div class="content-header row">
			<div class="content-header-left col-md-9 col-12 mb-2">
				<div class="row breadcrumbs-top">
					<div class="col-12">
						<h2 class="content-header-title float-left mb-0">รายการงานวิ่ง</h2>
						<div class="breadcrumb-wrapper col-12">
							<ol class="breadcrumb">
								<li class="breadcrumb-item active">รายการงานวิ่งล่าสุด
								</li>
							</ol>
						</div>
					</div>
				</div>
			</div>

		</div>
		<div class="content-body">
			<!-- Data list view starts -->
			<section id="data-list-view" class="data-list-view-header">
				<div class="action-btns d-none">
					<div class="btn-dropdown mr-1 mb-1">
						<div class="btn-group dropdown actions-dropodown">
							<button type="button" class="btn btn-white px-1 py-1 dropdown-toggle waves-effect waves-light" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								Actions
							</button>
							<div class="dropdown-menu">
								<a class="dropdown-item" href="#"><i class="feather icon-trash"></i>Delete</a>
								<a class="dropdown-item" href="#"><i class="feather icon-archive"></i>Archive</a>
								<a class="dropdown-item" href="#"><i class="feather icon-file"></i>Print</a>
								<a class="dropdown-item" href="#"><i class="feather icon-save"></i>Another Action</a>
							</div>
						</div>
					</div>
				</div>

				<!-- DataTable starts -->
				<div class="table-responsive">
					<table class="table data-list-view-order">
						<thead>
							<tr>
								<th></th>
								<th style="width: 10%">รูปภาพสลิป</th>
								<th>ชื่องานวิ่ง</th>
								<th>ลูกค้า</th>
								<th>ขนาดงานวิ่ง</th>
								<th>ราคารวม</th>
								<th>เลขที่ทำรายการ</th>
								<th>วัน-เวลาที่ทำรายการ</th>
								<th>สถานะการชำระเงิน</th>
								<th>เครื่องมือ</th>
							</tr>
						</thead>
						<tbody>
							<?php foreach ($event as $event) { ?>
								<?php $order = $this->db->get_where('tbl_order', ['event_id' => $event['id']])->row_array(); ?>
								<tr>
									<td></td>
									<td class="product-name"><a href="../uploads/payment/<?php echo $order['file_name']; ?>"><img src="../uploads/payment/<?php echo $order['file_name']; ?>" alt="" style="width: 150px"></a></td>

									<td class="product-name"><?php echo $event['name_event'];  ?></td>
									<?php $member = $this->db->get_where('tbl_member', ['id' => $event['member_id']])->result_array(); ?>
									<?php foreach ($member as $member) { ?>
										<td class="product-price"><?php echo $member['first_name'] . ' ' . $member['last_name'];  ?></td>
									<?php } ?>
									<?php $type = $this->db->get_where('tbl_type', ['id' => $event['type_event']])->result_array(); ?>
									<?php foreach ($type as $type) { ?>
										<?php $type_v = explode(' ', $type['name']); ?>
										<td class="product-price"><?php echo $type_v[0] . ' ' . $type_v[1];  ?></td>

										<td class="product-price"><?php echo $type['price'];  ?></td>
									<?php } ?>
									<td class="product-price"><?php echo $order['reference'];  ?></td>
									<td class="product-price"><?php echo $order['time'];  ?></td>
									<?php if ($event['status'] == 1) : ?>
										<td>
											<div class="dropdown ">
												<button class="btn btn-warning dropdown-toggle mr-1" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
													กำลังดำเนินการตรวจสอบ
												</button>
												<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
													<a class="dropdown-item" href="Admin_status_order?id=<?php echo $event['id']; ?>&status=2">อนุมัติ</a>
													<a class="dropdown-item" href="Admin_status_order?id=<?php echo $event['id']; ?>&status=3">ไม่อนุมัติ</a>
													<a class="dropdown-item" href="Admin_status_order?id=<?php echo $event['id']; ?>&status=0">กรุณาชำระเงิน</a>

												</div>
											</div>
										</td>

									<?php elseif ($event['status'] == 0) : ?>
										<td>
											<div class="dropdown ">
												<button class="btn btn-warning dropdown-toggle mr-1" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
													กรุณาชำระเงิน
												</button>
												<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
													<a class="dropdown-item" href="Admin_status_order?id=<?php echo $event['id']; ?>&status=1">กำลังดำเนินการตรวจสอบ</a>
													<a class="dropdown-item" href="Admin_status_order?id=<?php echo $event['id']; ?>&status=2">อนุมัติ</a>
													<a class="dropdown-item" href="Admin_status_order?id=<?php echo $event['id']; ?>&status=3">ไม่อนุมัติ</a>

												</div>
											</div>
										</td>
									<?php elseif ($event['status'] == 2) : ?>
										<td>
											<div class="dropdown ">
												<button class="btn btn-success dropdown-toggle mr-1" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
													อนุมัติ
												</button>
												<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
													<a class="dropdown-item" href="Admin_status_order?id=<?php echo $event['id']; ?>&status=1">กำลังดำเนินการตรวจสอบ</a>
													<a class="dropdown-item" href="Admin_status_order?id=<?php echo $event['id']; ?>&status=3">ไม่อนุมัติ</a>
													<a class="dropdown-item" href="Admin_status_order?id=<?php echo $event['id']; ?>&status=0">กรุณาชำระเงิน</a>
												</div>
											</div>
										</td>

									<?php else : ?>
										<td>
											<div class="dropdown ">
												<button class="btn btn-danger dropdown-toggle mr-1" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
													ไม่อนุมัติ
												</button>
												<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
													<a class="dropdown-item" href="Admin_status_order?id=<?php echo $event['id']; ?>&status=2">อนุมัติ</a>
													<a class="dropdown-item" href="Admin_status_order?id=<?php echo $event['id']; ?>&status=1">กำลังดำเนินการตรวจสอบ</a>
													<a class="dropdown-item" href="Admin_status_order?id=<?php echo $event['id']; ?>&status=0">กรุณาชำระเงิน</a>
												</div>
											</div>
										</td>

									<?php endif; ?>

									<td class="product-action">
										<a href="Admin_Order_user?id=<?php echo $event['id']; ?>"><i class="feather icon-user-check" style="font-size: 25px;"></i></a>
									</td>
								</tr>
							<?php  } ?>
						</tbody>
					</table>
				</div>
				<!-- DataTable ends -->


			</section>
			<!-- Data list view end -->

		</div>
	</div>
</div>
<!-- END: Content-->