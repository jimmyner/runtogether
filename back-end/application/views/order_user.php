<style>
    .map {
        height: 250px;
        width: 100%
    }
</style>

<!-- BEGIN: Content-->
<div class="app-content content">
    <div class="content-overlay"></div>
    <div class="header-navbar-shadow"></div>
    <div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h2 class="content-header-title float-left mb-0">รายการผู้ใช้งานวิ่ง</h2>
                        <div class="breadcrumb-wrapper col-12">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item active">รายการผู้ใช้งานวิ่งล่าสุด
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <div class="content-body">
            <!-- Data list view starts -->
            <section id="data-list-view" class="data-list-view-header">
                <div class="action-btns d-none">
                    <div class="btn-dropdown mr-1 mb-1">
                        <div class="btn-group dropdown actions-dropodown">
                            <button type="button" class="btn btn-white px-1 py-1 dropdown-toggle waves-effect waves-light" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Actions
                            </button>
                            <div class="dropdown-menu">
                                <a class="dropdown-item" href="#"><i class="feather icon-trash"></i>Delete</a>
                                <a class="dropdown-item" href="#"><i class="feather icon-archive"></i>Archive</a>
                                <a class="dropdown-item" href="#"><i class="feather icon-file"></i>Print</a>
                                <a class="dropdown-item" href="#"><i class="feather icon-save"></i>Another Action</a>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- DataTable starts -->
                <div class="table-responsive">
                    <table class="table data-list-view-order">
                        <thead>
                            <tr>
                                <th></th>
                                <th>ชื่อ-สกุล</th>
                                <th>บัตรประชาชน</th>
                                <th>รุ่นอายุ</th>
                                <th>เบอร์โทร</th>
                                <th>ประเภท</th>
                                <th>ขนาด(รอบอก)</th>
                                <th>วัน-เวลาที่ทำรายการ</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($register_run as $register_run) { ?>
                                <?php $order = $this->db->get_where('tbl_order', ['event_id' => $register_run['id']])->row_array(); ?>
                                <tr>
                                    <td></td>
                                    <td class="product-name"><?php echo $register_run['full_name'] ?></td>

                                    <td class="product-name"><?php echo $register_run['id_card'] ?></td>
                                 <?php $age_rank_id = $this->db->get_where('tbl_age', ['id' => $register_run['age_rank_id']])->row_array(); ?>
                                    <td class="product-price"><?php echo $age_rank_id['age_title'] ?></td>

                                    <td class="product-price"><?php echo $register_run['tel'] ?></td>
                                <?php $marathon_id = $this->db->get_where('tbl_marathon', ['id' => $register_run['marathon_id']])->row_array(); ?>
                                    <td class="product-price"><?php echo $marathon_id['name_marathon'] ?></td>
                                    <td class="product-price"><?php echo $register_run['size'] ?></td>
                                    <td class="product-price"><?php echo $register_run['create_at'] ?></td>

                                </tr>
                            <?php  } ?>
                        </tbody>
                    </table>
                </div>
                <!-- DataTable ends -->


            </section>
            <!-- Data list view end -->

        </div>
    </div>
</div>
<!-- END: Content-->