    <!-- BEGIN: Content-->
    <div class="app-content content">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper">
            <div class="content-header row">
                <div class="content-header-left col-md-9 col-12 mb-2">
                    <div class="row breadcrumbs-top">
                        <div class="col-12">
                            <h2 class="content-header-title float-left mb-0">usrt List</h2>
                            <div class="breadcrumb-wrapper col-12">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item active">User List
                                    </li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <div class="content-body">
                <!-- Data list view starts -->
                <section id="data-thumb-view" class="data-thumb-view-header">

                    <!-- dataTable starts -->
                    <div class="table-responsive">
                        <table class="table data-thumb-view-rider">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th>ชื่อ</th>
                                    <th>เบอร์โทร</th>
                                    <th>อีเมล</th>
                                
                                </tr>
                            </thead>
                            <tbody>
                                <?php $member = $this->db->get('tbl_member')->result_array(); ?>
                                <?php foreach ($member as $key => $member) { ?>
                                    <tr>
                                        <td></td>

                                        <td class="product-name"><?php echo $member['first_name'] . ' ' . $member['last_name'] ?></td>

                                        <td class="product-name"><?php echo $member['tel']; ?></td>
                                        <td class="product-name"><?php echo $member['email']; ?></td>

                                       
                                    </tr>
                                    <!-- Modal -->
                                    <div class="modal fade" id="exampleModal<?php echo $member['id']; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="exampleModalLabel">ไรเดอร์</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <form action="Admin_Rider_edit_com" method="POST" class="form-horizontal" enctype="multipart/form-data" novalidate>
                                                    <div class="modal-body">

                                                        <input type="hidden" class="form-control" name="id" value="<?php echo $member['id']; ?>">

                                                    </div>
                                                    <div class="modal-footer">
                                                        <div class="add-data-footer d-flex justify-content-around px-3 mt-2">
                                                            <div class="add-data-btn mr-1">
                                                                <button type="submit" class="btn btn-primary">แก้ไขข้อมูล</button>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </form>
                                            </div>

                                        </div>
                                        <script type="text/javascript">
                                            function readURL_edit<?php echo $rider['id']; ?>(input) {
                                                if (input.files && input.files[0]) {
                                                    var reader = new FileReader();

                                                    reader.onload = function(e) {
                                                        $('#blah_edit<?php echo $rider['id']; ?>').attr('src', e.target.result);
                                                    }

                                                    reader.readAsDataURL(input.files[0]);
                                                }
                                            }
                                        </script>
                                        <!-- End Modal -->
                                    <?php  } ?>
                            </tbody>
                        </table>
                    </div>
                    <!-- dataTable ends -->

                    <!-- add new sidebar starts -->
                    <div class="add-new-data-sidebar">
                        <div class="overlay-bg"></div>
                        <div class="add-new-data" style="overflow-y: scroll;">
                            <div class="div mt-2 px-2 d-flex new-data-title justify-content-between">
                                <div>
                                    <h4 class="text-uppercase">Rider</h4>
                                </div>
                                <div class="hide-data-sidebar">
                                    <i class="feather icon-x"></i>
                                </div>
                            </div>
                            <form action="Admin_Rider_com" method="POST" enctype="multipart/form-data">
                                <div class="data-items pb-3">
                                    <div class="data-fields px-2 mt-3">
                                        <div class="row">
                                            <div class="col-sm-12 data-field-col">
                                                <label for="data-category">คำนำหน้า</label>
                                                <select class="form-control" name="title">
                                                    <option value="นาย">นาย</option>
                                                    <option value="นางสาว">นางสาว</option>
                                                    <option value="นาง">นาง</option>
                                                </select>
                                            </div>
                                            <div class="col-sm-12 data-field-col">
                                                <label for="data-name">ชื่อ</label>
                                                <input type="text" class="form-control" name="first_name" required>
                                            </div>
                                            <div class="col-sm-12 data-field-col">
                                                <label for="data-name">นามสกุล</label>
                                                <input type="text" class="form-control" name="last_name" required>
                                            </div>
                                            <div class="col-sm-12 data-field-col">
                                                <label for="data-name">เลขบัตรประชาชน</label>
                                                <input type="text" class="form-control" name="id_card" required>
                                            </div>
                                            <div class="col-sm-12 data-field-col">
                                                <label for="data-name">เบอร์โทร</label>
                                                <input type="text" class="form-control" name="tel" required>
                                            </div>

                                            <div class="col-sm-12 data-field-col">
                                                <label for="data-name">อีเมล</label>
                                                <input type="email" class="form-control" name="email" required>
                                            </div>

                                            <div class="col-lg-12 col-md-12 data-field-col">
                                                <fieldset class="form-group">
                                                    <label for="basicInputFile">รูปไรเดอร์</label>
                                                    <div class="custom-file">
                                                        <input type="file" name="file_name" class="custom-file-input" onchange="readURL(this);" id="inputGroupFile01" />
                                                        <label class="custom-file-label" for="inputGroupFile01" style="overflow: hidden;">กรุณาเลือกไฟล์</label>
                                                        <div style="width: 115px;margin: 15px auto 0;">
                                                            <img id="blah" style="max-width:100%;" src="" alt="" />
                                                        </div>
                                                    </div>
                                                </fieldset>
                                            </div>


                                        </div>
                                    </div>
                                </div>
                                <div class="add-data-footer d-flex justify-content-around px-3 mt-2 form-group">
                                    <div class="add-data-btn">
                                        <button class="btn btn-primary">เพิ่มข้อมูล</button>
                                    </div>
                                </div>
                        </div>
                    </div>
                    </form>
                    <!-- add new sidebar ends -->
                </section>
                <!-- Data list view end -->

            </div>
        </div>
    </div>
    <!-- END: Content-->
    <script type="text/javascript">
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function(e) {
                    $('#blah').attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
            }
        }
    </script>