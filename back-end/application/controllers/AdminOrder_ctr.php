<?php
defined('BASEPATH') or exit('No direct script access allowed');

class AdminOrder_ctr extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        if ($this->session->userdata('username') == '') {
            redirect('Admin_Login');
     } else {
        $data['event'] = $this->db->get('tbl_event')->result_array();

        $this->load->view('option/header');
        $this->load->view('order', $data);
        $this->load->view('option/footer');
     }
    }


    public function order_user()
    {
        if ($this->session->userdata('username') == '') {
            redirect('Admin_Login');
     } else {

                               $id = $this->input->get('id');
        $data['register_run'] = $this->db->get_where('tbl_register_run',['id_event'=>$id])->result_array();
        $this->load->view('option/header');
        $this->load->view('order_user', $data);
        $this->load->view('option/footer');
     }
    }

  
  
    public function status_order()
    {
        $id = $this->input->get('id');
        $status = $this->input->get('status');

        $this->db->where('id', $id);
        $resultsedit = $this->db->update('tbl_event',['status' => $status]);

        if($resultsedit > 0)
        {
            $this->session->set_flashdata('save_ss2',' Successfully updated status information !!.');
        }
        else
        {
            $this->session->set_flashdata('del_ss2','Not Successfully updated status information');
        }
        return redirect('Admin_Order');
    }

}
