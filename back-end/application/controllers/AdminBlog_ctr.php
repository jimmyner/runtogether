<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AdminBlog_ctr extends CI_Controller {

	public function __construct()
  	{
		parent::__construct();
		
    }
  
	public function blog()
	{
        if ($this->session->userdata('username') == '') {
            redirect('Admin_Login');
     } else {
        $this->load->view('option/header');
        $this->load->view('blog');
        $this->load->view('option/footer');
     } 
    }

    public function add_blog()
	{
        if ($this->session->userdata('username') == '') {
            redirect('Admin_Login');
     } else {
        $this->load->view('option/header');
        $this->load->view('add_blog');
        $this->load->view('option/footer');
     } 
    }

    public function blog_add_com()
    {

         $this->load->library('upload');
      
        // |xlsx|pdf|docx
        $config['upload_path'] = '../uploads/blog';
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['max_size']     = '200480';
        $config['max_width'] = '5000';
        $config['max_height'] = '5000';
        $name_file = "blog-".time();
        $config['file_name'] = $name_file;

        $this->upload->initialize($config);

        $data = array();

            if ($_FILES['file_name']['name']) {           
                if ($this->upload->do_upload('file_name')){

                    $gamber     = $this->upload->data();
                    $data = array
                    (
                        'file_name'     => $gamber['file_name'],
                        'name'          => $this->input->post('name') , 
                        'details'          => $this->input->post('details') , 
                        'type'          => $this->input->post('type') , 
                        'create_at'     => date('Y-m-d H:i:s') 

                    );
                      $resultsedit = $this->db->insert('tbl_blog',$data);
                }
               
            }
          
            if($resultsedit > 0)
            {
                $this->session->set_flashdata('save_ss2','เพิ่มข้อมูลบทความละข่าวสารเรียบร้อยแล้ว !!.');
            }
            else
            {
                $this->session->set_flashdata('del_ss2','ไม่สามารถเพิ่มข้อมูลบทความละข่าวสารได้');
            }
            return redirect('Admin_Blog');
    }

    public function edit_blog()
    {
        $this->load->library('upload');
      
        // |xlsx|pdf|docx
        $config['upload_path'] = '../uploads/blog';
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['max_size']     = '200480';
        $config['max_width'] = '5000';
        $config['max_height'] = '5000';
        $name_file = "blog-".time();
        $config['file_name'] = $name_file;

        $this->upload->initialize($config);

        $data = array();

        if (empty($_FILES['file_name']['name'])) 
        {
                    $data = array
                    (
                        'name'          => $this->input->post('name') , 
                        'details'          => $this->input->post('details') , 
                        'type'          => $this->input->post('type') , 
                        'create_at'     => date('Y-m-d H:i:s') 

                    );
                    $this->db->where('id',$this->input->post('id'));
                    $resultsedit = $this->db->update('tbl_blog',$data);
        }else{
            if ($_FILES['file_name']['name']) {           
                if ($this->upload->do_upload('file_name')){

                    $gamber     = $this->upload->data();
                    $data = array
                    (
                        'file_name'     => $gamber['file_name'],
                        'name'          => $this->input->post('name') , 
                        'details'          => $this->input->post('details') , 
                        'type'          => $this->input->post('type') , 
                        'create_at'     => date('Y-m-d H:i:s') 

                    );
                    $this->db->where('id',$this->input->post('id'));
                    $resultsedit = $this->db->update('tbl_blog',$data);
                }
               
            }
        }

            if($resultsedit > 0)
            {
                $this->session->set_flashdata('save_ss2','แก้ไขข้อมูลบทความละข่าวสารร้อยแล้ว !!.');
            }
            else
            {
                $this->session->set_flashdata('del_ss2','ไม่สามารถแก้ไขข้อมูลบทความละข่าวสารได้');
            }
            redirect('Admin_Blog');
    }
    
    public function delete_blog()
    {
        $id = $this->input->get('id');

        $this->db->where('id',$id);
        $results_delete = $this->db->delete('tbl_blog');

        if($results_delete > 0)
        {
            $this->session->set_flashdata('save_ss2','ลบข้อมูลบทความละข่าวสารเรียบร้อยแล้ว !!.');
        }
        else
        {
            $this->session->set_flashdata('del_ss2','ไม่สามารถลบข้อมูลบทความละข่าวสารได้');
        }
        redirect('Admin_Blog');

    }
    public function comment()
	{
       
        if ($this->session->userdata('username') == '') {
            redirect('Admin_Login');
     } else {
        $this->load->view('option/header');
        $this->load->view('comment');
        $this->load->view('option/footer');
     }
          
    }

   
    
  

}
