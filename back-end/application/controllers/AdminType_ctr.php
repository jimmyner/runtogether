<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AdminType_ctr extends CI_Controller {

	public function __construct()
  	{
		parent::__construct();
		$this->load->model('Type_model');
    }
  
	public function index()
	{
        if ($this->session->userdata('username') == '') {
            redirect('Admin_Login');
     } else {
        $this->load->view('option/header');
        $this->load->view('type');
        $this->load->view('option/footer');
     } 
    }

    public function type_com()
    {
        $name                = $this->input->post('name');
        $price               = $this->input->post('price');
        $people              = $this->input->post('people');

        
            $data = array(
                'name'                  => $name,
                'price'                 => $price,
                'people'                => $people,
       		
               
               
            );
            $success = $this->db->insert('tbl_type',$data);
        
            if($success > 0)
            {
                $this->session->set_flashdata('save_ss2','เพิ่มข้อมูลประเภทขนาดงานวิ่งเรียบร้อยแล้ว !!.');
            }else
            {
                $this->session->set_flashdata('del_ss2','ไม่สามารถเพิ่มข้อมูลได้');
            }
                redirect('Admin_Type');
        
    }

    public function delete_type()
    {

        $id = $this->input->get('id');

        if ($this->Type_model->type_delete($id))
        {
            $this->session->set_flashdata('del_ss2','ไม่สามารถลบข้อมูลได้ !!.');
        }
        else
        {
            $this->session->set_flashdata('save_ss2','ลบข้อมูลประเภทอาหารเรียบร้อยแล้ว');
        }
        return redirect('Admin_Type');
    }

    public function edit_type()
    {

        $id                = $this->input->post('id');
        $name                = $this->input->post('name');
        $price               = $this->input->post('price');
        $people              = $this->input->post('people');

        
            $data = array(
                'name'                  => $name,
                'price'                 => $price,
                'people'                => $people,
       		
               
               
            );
        $this->db->where('id',$id);
        $success = $this->db->update('tbl_type',$data);
    
        if($success > 0)
        {
            $this->session->set_flashdata('save_ss2','เพิ่มข้อมูลประเภทอาหารเรียบร้อยแล้ว !!.');
        }else
        {
            $this->session->set_flashdata('del_ss2','ไม่สามารถเพิ่มข้อมูลได้');
        }
            redirect('Admin_Type');
    }
  

}
