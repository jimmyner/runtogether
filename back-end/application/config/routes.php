<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'AdminLogin_ctr';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

$route['Allrun'] = 'Run_ctr/allrun';
$route['Admin_Login']                   = 'AdminLogin_ctr';
$route['Admin_Login_suss']              = 'AdminLogin_ctr/admin_loginMe';
$route['Admin_Logout']                  = 'AdminLogin_ctr/admin_logout';
$route['Admin_Order']                   = 'AdminOrder_ctr'; 
$route['Admin_Order_user']              = 'AdminOrder_ctr/order_user'; 
$route['Admin_status_order']            = 'AdminOrder_ctr/status_order'; 
$route['Admin_user']                    = 'AdminUser_ctr'; 
$route['Admin_Type']                    = 'AdminType_ctr'; 
$route['Admin_Type_com']                = 'AdminType_ctr/type_com'; 
$route['Admin_edit_type_com']           = 'AdminType_ctr/edit_type'; 
$route['delete_type']                   = 'AdminType_ctr/delete_type'; 
$route['Admin_Blog']                    = 'AdminBlog_ctr/blog'; 
$route['Admin_Blog_Add']                = 'AdminBlog_ctr/add_blog'; 
$route['blog_add_com']                  = 'AdminBlog_ctr/blog_add_com'; 
$route['edit_blog']                  = 'AdminBlog_ctr/edit_blog'; 
$route['delete_blog']                  = 'AdminBlog_ctr/delete_blog'; 
$route['Admin_Blog_Comment']            = 'AdminBlog_ctr/comment'; 

// 