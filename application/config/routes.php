<?php
defined('BASEPATH') or exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'Index_ctr/home';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

$route['Index']              = 'Index_ctr/home';
// $route['pang']               = 'Index_ctr/pang';
$route['event-details']      = 'Index_ctr/event_details';
$route['send-event']         = 'Index_ctr/send_event';
$route['Allrun']             = 'Run_ctr/allrun';
$route['allrun_search']      = 'Run_ctr/allrun_search';
$route['passpost']           = 'Passpost_ctr/passpost_search';
$route['Login']              = 'Login_ctr';
$route['LoginMe']            = 'Login_ctr/loginMe';
$route['Logout']             = 'Login_ctr/logout';
$route['Contact']            = 'Contact_ctr/contact';
$route['ContacMe']           = 'Contact_ctr/contacMe';
$route['Blog-news']          = 'Blog_ctr/blog_news';
$route['Blog-article']       = 'Blog_ctr/blog_article';
$route['Blog-detail']        = 'Blog_ctr/blog_detail';
$route['About-us']           = 'About_ctr/about';
$route['Register']           = 'Register_ctr/register';
$route['RegisterMe']         = 'Register_ctr/registerMe';

$route['My-profile']         = 'Users_ctr/my_profile';
$route['My-profile-update']  = 'Users_ctr/my_profile_update';
$route['my-activity']        = 'Activity_ctr/my_activity';
$route['my-activity-create'] = 'Activity_ctr/my_activity_create';
$route['my-order']           = 'Order_ctr/my_order';
$route['my-register-view']   = 'Order_ctr/my_register_view';
$route['my-register-status'] = 'Order_ctr/my_register_status';
$route['my-activity-view']   = 'Order_ctr/my_activity_view';

$route['my-payment']         = 'Payment_ctr/my_payment';
$route['my-payment-create']  = 'Payment_ctr/my_payment_create';
