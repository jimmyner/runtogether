<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/* Author: Jorge Torres
 * Description: Login model class
 */
class Order_model extends CI_Model
{
    function __construct(){
        parent::__construct();
    }
    
    public function order($id)
    {
        $this->db->select('*,tbl_event.id AS idevent');
        $this->db->from('tbl_event');
        $this->db->join('tbl_member', 'tbl_member.id = tbl_event.member_id','left');
        $this->db->join('tbl_type', 'tbl_type.id = tbl_event.type_event','left');
        $this->db->where('tbl_event.member_id', $id);
        $query = $this->db->get();

        return $query->result();

    }

}

?>