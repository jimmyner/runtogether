<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/* Author: Jorge Torres
 * Description: Login model class
 */
class Search_model extends CI_Model
{
    function __construct(){
        parent::__construct();
    }
    
    public function search($keyword)
    {
        $this->db->like('name_event',$keyword);
        $this->db->where('status','2');
        $query = $this->db->get('tbl_event');
        return $query->result_array();
    }

    public function search_main($passpost)
    {
        $this->db->select('*,tbl_register_run.create_at AS run_at , tbl_register_run.status AS status_run ');
        $this->db->from('tbl_register_run');
        $this->db->join('tbl_event', 'tbl_register_run.id_event = tbl_event.id');
        $this->db->join('tbl_marathon', 'tbl_register_run.marathon_id = tbl_marathon.id');
        $this->db->where('tbl_register_run.id_card',$passpost);
        $query = $this->db->get();
        return $query->result_array();
    }

}

?>