<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Register_ctr extends CI_Controller {

    public function __construct()
	{
		parent::__construct();
	}

	public function register()
	{
		$this->load->view('option/header');
		$this->load->view('register');
		$this->load->view('option/footer');
	}

	public function registerMe()
	{
		$data = array 
		(
			'first_name' 	=> $this->input->post('f_name') ,//f_name เป็นชื่อ input // ช่องกรอกข้อมูล (name)
			'last_name' 	=> $this->input->post('l_name') ,
			'tel' 			=> $this->input->post('tel') ,
			'gender' 		=> $this->input->post('Gender') ,
			'email' 		=> $this->input->post('email') ,
			'password' 		=> md5($this->input->post('password')) ,
			'create_at' 	=> date('Y-m-d H:i:s') 
		);

		$success = $this->db->insert('tbl_member', $data);
		if ($success > 0) 
		{
			echo "<script>";
			echo "alert('สมัครสมาชิกเรียบร้อย ยินดีต้อนรับเข้าสู่เว็บไซต์ Runtogether.');";
			echo "window.location='Login'";
			echo "</script>"; 
		}else{
			echo "<script>";
			echo "alert('สมัครสมาชิกไม่สำเร็จ กรุณาลองใหม่อีกครั้ง!.');";
			echo "window.location='Register'";
			echo "</script>"; 
		}
		
	}
}
