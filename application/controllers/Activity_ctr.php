<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Activity_ctr extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    public function my_activity()
    {
        if ($this->session->userdata('email') != '') {
            $this->load->view('option/header');
            $this->load->view('my_activity');
            $this->load->view('option/footer');
        } else {
            redirect('Login');
        }
    }

    public function my_activity_create()
    {
        $this->load->library('upload');

        // |xlsx|pdf|docx
        $config['upload_path'] = 'uploads/event';
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['max_size']     = '200480';
        $config['max_width'] = '5000';
        $config['max_height'] = '5000';
        $name_file = "event-" . time();
        $config['file_name'] = $name_file;

        $this->upload->initialize($config);

       
        $data = array();

        if ($_FILES['file_nameA']['name']) {
            
            if ($this->upload->do_upload('file_nameA')) 
            {

                $gamber             = $this->upload->data();

                $data = array(
                    'file_cover'        => $gamber['file_name'],
                    'member_id'         => $this->input->post('member_id'),
                    'name_event'        => $this->input->post('event_name'),
                    'details_event'     => $this->input->post('detail'),
                    'type_event'        => $this->input->post('package'),
                    'account_number'    => $this->input->post('account_number'),
                    'size_s'            => $this->input->post('S'),
                    'size_m'            => $this->input->post('M'),
                    'size_l'            => $this->input->post('L'),
                    'size_xl'           => $this->input->post('XL'),
                    'size_oversize'     => $this->input->post('over'),
                    'time_out'          => $this->input->post('date'),
                    'bank'              => $this->input->post('bank'),
                    'create_at'         => date('Y-m-d H:i:s')
                );
                $this->db->insert('tbl_event', $data);
                $insert_id = $this->db->insert_id(); // เอา id ล่าสุด ที่ save ก่อนหน้านี้มาใช้ต่อ
            }
            if($this->input->post('fun'))
            {
                if ($this->input->post('km_fun') && $this->input->post('price_fun')) {
                    $runfun = array(
                        'id_event'      =>  $insert_id ,
                        'name_marathon' =>  $this->input->post('fun') ,
                        'length'        =>  $this->input->post('km_fun') ,
                        'price'         =>  $this->input->post('price_fun') ,
                        'create_at'     => date('Y-m-d H:i:s')
                    );  
                $this->db->insert('tbl_marathon', $runfun);
                }else{
                    echo "<script>";
                    echo "alert('ท่านไม่ได้กรอก fun marathons');";
                    echo "window.location='my-activity'";
                    echo "</script>";
                }
                
            }
            if ($this->input->post('mini') ) 
            {
                if ($this->input->post('km_mini') && $this->input->post('price_mini')) {
                    $runmini = array(
                        'id_event'      =>  $insert_id ,
                        'name_marathon' =>  $this->input->post('mini') ,
                        'length'        =>  $this->input->post('km_mini') ,
                        'price'         =>  $this->input->post('price_mini') ,
                        'create_at'     => date('Y-m-d H:i:s')
                    );
                    $this->db->insert('tbl_marathon', $runmini);
                }else{
                    echo "<script>";
                    echo "alert('ท่านไม่ได้กรอก mini marathons');";
                    echo "window.location='my-activity'";
                    echo "</script>";
                }
                
            }
            if ($this->input->post('half')) 
            {
                if ($this->input->post('km_half') && $this->input->post('price_half')) {
                    $runhalf = array(
                        'id_event'      =>  $insert_id ,
                        'name_marathon' =>  $this->input->post('half') ,
                        'length'        =>  $this->input->post('km_half') ,
                        'price'         =>  $this->input->post('price_half') ,
                        'create_at'     => date('Y-m-d H:i:s')
                    );
                    $this->db->insert('tbl_marathon', $runhalf);
                }else{
                    echo "<script>";
                    echo "alert('ท่านไม่ได้กรอก half marathons');";
                    echo "window.location='my-activity'";
                    echo "</script>";
                }
                
            }

            if ($age = $this->input->post('age')) {
                foreach ($age as $key => $age) {
                    $dataage = array(
                        'id_event'      =>  $insert_id ,
                        'age_title'     =>  $age ,
                        'create_at'     =>  date('Y-m-d H:i:s')
                    );
                    $this->db->insert('tbl_age', $dataage);
                }
                
            }

            if ($_FILES['file_nameB']['name']) 
            {
                 // |xlsx|pdf|docx
                $configs['upload_path'] = 'uploads/event';
                $configs['allowed_types'] = 'gif|jpg|png|jpeg';
                $configs['max_size']     = '200480';
                $configs['max_width'] = '5000';
                $configs['max_height'] = '5000';
                $name_files = "shirt-" . time();
                $configs['file_name'] = $name_files;

                $this->upload->initialize($configs);
                if ($this->upload->do_upload('file_nameB')) 
                {
                    $gambers             = $this->upload->data();

                    $datas = array(
                        'file_shirt'    => $gambers['file_name'],
                    );
                    $this->db->where('id', $insert_id);
                    $resultsedit = $this->db->update('tbl_event', $datas);
                }
            }
        }
        
        if ($resultsedit > 0) {
            echo "<script>";
            echo "alert('คุณได้ทำการอัพเดทข้อมูลเรียบร้อยแล้ว.');";
            echo "window.location='my-activity'";
            echo "</script>";
        } else {
            echo "<script>";
            echo "alert('ไม่สามารถรอัพเดทข้อมูลได้ กรุณาลองใหม่อีกครั้ง !!!');";
            echo "window.location='my-activity'";
            echo "</script>";
        }
    }
}
