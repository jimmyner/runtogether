<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Order_ctr extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Order_model');
    }

    public function my_order()
    {
        if ($this->session->userdata('email') != '') {
            $user = $this->db->get_where('tbl_member', ['email' => $this->session->userdata('email')])->row_array();
            $id = $user['id'];
            $data['order'] = $this->Order_model->order($id);
            $this->load->view('option/header');
            $this->load->view('my_order', $data);
            $this->load->view('option/footer');
        } else {
            redirect('Login');
        }
    }

    public function my_activity_view()
    {
        if ($this->session->userdata('email') != '') {
            $this->load->view('option/header');
            $this->load->view('my_activity_view');
            $this->load->view('option/footer');
        } else {
            redirect('Login');
        }
    }

    public function my_register_view()
    {
        if ($this->session->userdata('email') != '') {
            $this->load->view('option/header');
            $this->load->view('my_register_view');
            $this->load->view('option/footer');
        } else {
            redirect('Login');
        }
    }

    public function my_register_status()
    {
        if ($this->session->userdata('email') != '') {
            $id         = $this->input->get('id');
            $status     = $this->input->get('status');
            $id_event   = $this->input->get('id_event');

            $data = array(
                'status'    => $status,
            );
            $this->db->where('id', $id);
            if ($this->db->update('tbl_register_run', $data)) {
                echo "<script>";
                echo "alert('ทำการอนุมัติการสมัครงานวิ่งเรียบร้อยแล้ว.');";
                echo "window.location='my-register-view?id=$id_event'";
                echo "</script>";
            } else {
                echo "<script>";
                echo "alert('เกิดข้อผิดพลาดของระบบ กรุณาลองใหม่อีกครั้ง !!');";
                echo "window.location='my-register-view?id=$id_event'";
                echo "</script>";
            }
        } else {
            redirect('Login');
        }
    }
}
