<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Passpost_ctr extends CI_Controller {

    public function __construct()
	{
		parent::__construct();		
		$this->load->model('Search_model');	
	}

	public function passpost_search()
	{

		$passpost = $this->input->get('passpost');
        $data['passpost'] = $this->Search_model->search_main($passpost);
		if ($data['passpost'] == false) {
			echo "<script>";
            echo "alert('ไม่พบสิ่งที่คุณหา กรุณาค้นหาใหม่.');";
            echo "window.location='Index'";
            echo "</script>";
		}else{
			$this->load->view('option/header');
			$this->load->view('passpost',$data);
			$this->load->view('option/footer');
		}
       
	}
}
