<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Blog_ctr extends CI_Controller {

    public function __construct()
	{
		parent::__construct();
	}

	public function blog_article()
	{
		$this->load->view('option/header');
		$this->load->view('blog_article');
		$this->load->view('option/footer');
	}

	public function blog_news()
	{
		$this->load->view('option/header');
		$this->load->view('blog_news');
		$this->load->view('option/footer');
	}

	public function blog_detail()
	{
		$this->load->view('option/header');
		$this->load->view('blog_detail');
		$this->load->view('option/footer');
	}
}
