<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Run_ctr extends CI_Controller {

    public function __construct()
	{
		parent::__construct();		
		$this->load->model('Search_model');	
	}

	public function allrun()
	{
		$this->load->view('option/header');
		$this->load->view('allrun');
		$this->load->view('option/footer');
	}

	public function allrun_search()
	{

		$keyword = $this->input->get('keyword');
        $data['results'] = $this->Search_model->search($keyword);
		if ($data['results'] == false) {
			echo "<script>";
            echo "alert('ไม่พบสิ่งที่คุณหา กรุณาค้นหาใหม่.');";
            echo "window.location='Allrun'";
            echo "</script>";
		}else{
			$this->load->view('option/header');
			$this->load->view('allrun_search',$data);
			$this->load->view('option/footer');
		}
       
	}
}
