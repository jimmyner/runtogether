<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Payment_ctr extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    public function my_payment()
    {
        if ($this->session->userdata('email') != '') {
            $this->load->view('option/header');
            $this->load->view('my_payment');
            $this->load->view('option/footer');
        } else {
            redirect('Login');
        }
    }

    public function my_payment_create()
    {
        $this->load->library('upload');

        // |xlsx|pdf|docx
        $config['upload_path'] = 'uploads/payment';
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['max_size']     = '200480';
        $config['max_width'] = '5000';
        $config['max_height'] = '5000';
        $name_file = "payment-" . time();
        $config['file_name'] = $name_file;

        $this->upload->initialize($config);

       
        $data = array();

        if ($_FILES['file_name']['name']) {
            
            if ($this->upload->do_upload('file_name')) 
            {

                $gamber             = $this->upload->data();

                $data = array(
                    'file_name'         => $gamber['file_name'],
                    'member_id'         => $this->input->post('member_id'),
                    'event_id'          => $this->input->post('event_id'),
                    'reference'         => $this->input->post('number'),
                    'time'              => $this->input->post('time'),
                    'date'              => $this->input->post('date'),
                    'note'              => $this->input->post('detail'),
                    'create_at'         => date('Y-m-d H:i:s'),
                );
                if ($this->db->insert('tbl_order', $data)) {
                    $upadte = array(
                        'status'        => '1',
                    );
                    $this->db->where('id', $this->input->post('event_id'));
                    $resultsedit = $this->db->update('tbl_event', $upadte);
                    
                } 
            }
          
        }
        
        if ($resultsedit > 0) {
            echo "<script>";
            echo "alert('คุณได้ทำการชำระเงินเรียบร้อย กรุณารอระบบตรวจสอบ ภายใน 24 ชม.');";
            echo "window.location='my-order'";
            echo "</script>";
        } else {
            echo "<script>";
            echo "alert('ไม่สามารถรชำระเงินได้ กรุณาลองใหม่อีกครั้ง !!!');";
            echo "window.location='my-order'";
            echo "</script>";
        }
    }
}
