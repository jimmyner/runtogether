<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Login_ctr extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Login_model');
    }

    public function index()
    {
        if ($this->session->userdata('email') == '') {
            $this->load->view('option/header');
            $this->load->view('login');
            $this->load->view('option/footer');
        } else {
            redirect('My-profile');
        }
    }

    public function loginMe()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('email', 'email', 'required');
        $this->form_validation->set_rules('password', 'Password', 'required');
        if ($this->form_validation->run()) {
            $email      = $this->input->post('email');
            $password   = md5($this->input->post('password'));
            $this->load->model('Login_model');

            if ($this->Login_model->login($email, $password)) {
                $user_data = array(
                    'email' => $email
                );
                $this->session->set_userdata($user_data);
                echo "<script>";
                echo "alert('ยินดีต้อนรับเข้าสู่ระบบ.');";
                echo "window.location='My-profile'";
                echo "</script>";
            } else {
                echo "<script>";
                echo "alert('เข้าสู่ระบบไม่สำเร็จ กรุณาลองใหม่อีกครั้ง!!.');";
                echo "window.location='Login'";
                echo "</script>";
            }
        } else {
            echo "<script>";
            echo "alert('เข้าสู่ระบบไม่สำเร็จ กรุณาลองใหม่อีกครั้ง!!.');";
            echo "window.location='Login'";
            echo "</script>";
        }
    }

    public function logout()
    {
        $this->session->sess_destroy(); //ล้างsession

        echo "<script>";
        echo "alert('ออกจากระบบเรียบร้อยแล้ว.');";
        echo "window.location='Index'";
        echo "</script>";
    }
}
