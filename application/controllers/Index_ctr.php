<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Index_ctr extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Search_model');
	}

	public function home()
	{
		$this->load->view('option/header');
		$this->load->view('index');
		$this->load->view('option/footer');
	}

	// public function pang ()
	// {
	// 	echo "pang"; 
	// }

	public function event_details()
	{
		$this->load->view('option/header');
		$this->load->view('event-details');
		$this->load->view('option/footer');
	}

	public function send_event()
	{
		$get_id				= $this->input->post('get_id');
		$full_name 			= $this->input->post('full_name');
		$age 				= $this->input->post('age');
		$id_card 			= $this->input->post('card');
		$tel 				= $this->input->post('tel');
		$create_at 			= date('Y-m-d H:i:s');
		$marathon           = $this->input->post('marathon');
		$age_rank           = $this->input->post('age_rank');
		$size               = $this->input->post('size');

		$get_register_run 	= $this->db->get_where('tbl_register_run', ['id_card' => $id_card, 'id_event' => $get_id])->row_array();

		if ($get_register_run) {
			echo "<script>";
			echo "alert('ไม่สามารถสมัครงานวิ่งได้ เนื่องจากคุณเคยสมัครงานวิ่งนี้ไปแล้ว !!');";
			echo "window.location='event-details?id=$get_id'";
			echo "</script>";
		} else {
			$this->load->library('upload');

			// |xlsx|pdf|docx
			$config['upload_path'] = 'uploads/payment';
			$config['allowed_types'] = 'gif|jpg|png|jpeg';
			$config['max_size']     = '200480';
			$config['max_width'] = '5000';
			$config['max_height'] = '5000';
			$name_file = "payment-" . time();
			$config['file_name'] = $name_file;

			$this->upload->initialize($config);


			$data = array();

			if ($_FILES['file_name']['name']) {
				if ($this->upload->do_upload('file_name')) {

					$gamber             = $this->upload->data();

					$data = array(
						'id_event'		=> $get_id,
						'full_name'		=> $full_name,
						'age'			=> $age,
						'id_card'		=> $id_card,
						'marathon_id'   => $marathon,
						'age_rank_id'   => $age_rank,
						'size'		    => $size,
						'tel'			=> $tel,
						'file_name'		=> $gamber['file_name'],
						'status'		=> 1,
						'create_at'		=> $create_at
					);
					$success = $this->db->insert('tbl_register_run', $data);
					if ($success > 0) {
						echo "<script>";
						echo "alert('คุณได้ทำการสมัครงานวิ่งเรียบร้อยแล้ว ขอบคุณค่ะ.');";
						echo "window.location='event-details?id=$get_id'";
						echo "</script>";
					} else {
						echo "<script>";
						echo "alert('เกิดข้อผิดพลาดในการสมัครงานวิ่ง กรุณาลองใหม่อีกครั้งภายหลัง !!');";
						echo "window.location='event-details?id=$get_id'";
						echo "</script>";
					}
				}
			} else {
				echo "<script>";
				echo "alert('เกิดข้อผิดพลาดในการสมัครงานวิ่ง กรุณาลองใหม่อีกครั้งภายหลัง !!');";
				echo "window.location='event-details?id=$get_id'";
				echo "</script>";
			}
		}
	}
}
