<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contact_ctr extends CI_Controller {

    public function __construct()
	{
		parent::__construct();
	}

	public function contact()
	{
		$this->load->view('option/header');
		$this->load->view('contact');
		$this->load->view('option/footer');
	}

	public function contacMe()
	{
		$data = array 
		(
			'first_name' 	=> $this->input->post('f_name') ,
			'last_name' 	=> $this->input->post('l_name') ,
			'tel' 			=> $this->input->post('tel') ,
			'email' 		=> $this->input->post('email') ,
			'detail' 		=> $this->input->post('detail') ,
			'create_at' 	=> date('Y-m-d H:i:s') 
		);

		$success = $this->db->insert('tbl_contact', $data);
		if ($success > 0) 
		{
			echo "<script>";
			echo "alert('ส่ง contact ไปหา admin เรียบร้อยแล้ว.');";
			echo "window.location='Contact'";
			echo "</script>"; 
		}else{
			echo "<script>";
			echo "alert('ส่ง contact ไม่สำเร็จ กรุณาลองใหม่อีกครั้ง!.');";
			echo "window.location='Contact'";
			echo "</script>"; 
		}
		
	}
}
