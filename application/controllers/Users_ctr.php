<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Users_ctr extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Login_model');
    }

    public function my_profile()
    {
        if ($this->session->userdata('email') != '') {
            $this->load->view('option/header');
            $this->load->view('my_profile');
            $this->load->view('option/footer');
        } else {
            redirect('Login');
        }
    }

    public function my_profile_update()
    {
        $id                     = $this->input->post('id');
        $first_name             = $this->input->post('first_name');
        $last_name              = $this->input->post('last_name');
        $gender                 = $this->input->post('gender');
        $tel                    = $this->input->post('tel');
        $update_at              = date('Y-m-d H:i:s');

        $data = array(
            'first_name'        => $first_name,
            'last_name'         => $last_name,
            'gender'            => $gender,
            'tel'               => $tel,
            'update_at'         => $update_at,
        );
        $this->db->where('id', $id);
        if ($this->db->update('tbl_member', $data)) {
            echo "<script>";
            echo "alert('คุณได้ทำการอัพเดทข้อมูลเรียบร้อยแล้ว.');";
            echo "window.location='My-profile'";
            echo "</script>";
        } else {
            echo "<script>";
            echo "alert('ไม่สามารถรอัพเดทข้อมูลได้ กรุณาลองใหม่อีกครั้ง !!!');";
            echo "window.location='My-profile'";
            echo "</script>";
        }
    }
}
