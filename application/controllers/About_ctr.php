<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class About_ctr extends CI_Controller {

    public function __construct()
	{
		parent::__construct();
	}

	public function about()
	{
		$this->load->view('option/header');
		$this->load->view('about-us');
		$this->load->view('option/footer');
	}
}
