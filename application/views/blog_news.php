<?php $news = $this->db->get_where('tbl_blog', ['type' => 2])->result_array(); ?>
<?php $news_2 = $this->db->get_where('tbl_blog', ['type' => 2])->result_array(); ?>

<div class="intro-inner">
    <div class="about-intro" style="
    background:url(images/bg2.jpg) no-repeat center;
	background-size:cover;">
        <div class="dtable hw100">
            <div class="dtable-cell hw100">
                <div class="container text-center animated fadeInDown">
                    <h1 class="intro-title"> ข่าวสาร</h1>
                    <!-- <h2>Sign up to subscribe to email alerts and <br> you'll never miss a post. Subscribe Now !</h2> -->
                </div>
            </div>
        </div>
    </div>
    <!--/.about-intro -->
</div>
<!-- /.intro-inner -->

<div class="main-container inner-page">
    <div class="container">
        <div class="section-content">
            <div class="row ">
                <?php if (!empty($news)) : ?>
                <div class="col-sm-8 blogLeft">
                    <div class="blog-post-wrapper">

                        <?php foreach ($news as $key => $news) : ?>
                            <article class="blog-post-item">
                                <div class="inner-box">


                                    <!--blog image-->
                                    <div class="blog-post-img">

                                        <a href="Blog-detail?id=<?php echo $news['id']; ?>">
                                            <figure>
                                                <img class="img-responsive" alt="blog-post image" src="uploads/blog/<?php echo $news['file_name'];  ?>">
                                            </figure>
                                        </a>
                                    </div>

                                    <!--blog content-->

                                    <div class="blog-post-content-desc">


                                        <span class="info-row blog-post-meta"> <span class="date"><i class=" icon-clock"> </i> Today 1:21 pm </span> -
                                            <span class="author"> <i class="fa fa-user"></i> <a rel="author" title="Posts by Jhon Doe" href="#">Jhon
                                                    Doe</a> </span> -
                                            <span class="item-location"><i class="fa fa-comments"></i> Comments <a href="#">0</a> </span> </span>


                                        <div class="blog-post-content">
                                            <h2><a href="Blog-detail?id=<?php echo $news['id']; ?>"><?php echo $news['name'];  ?></a></h2>

                                            <div class="row">
                                                <div class="col-md-12 clearfix blog-post-bottom">
                                                    <a class="btn btn-primary  pull-left" href="Blog-detail?id=<?php echo $news['id']; ?>">อ่านต่อ..</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                </div>
                            </article>
                        <?php endforeach; ?>


                    </div>
                    <!--/.blog-post-wrapper-->
                </div>
                <!--blogLeft-->


                <div class="col-sm-4 blogRight page-sidebar">
                    <aside>
                        <div class="inner-box">
                            <div class="categories-list  list-filter">
                                <h5 class="list-title uppercase"><strong><a href="#"> Categories</a></strong></h5>
                                <ul class=" list-unstyled list-border ">
                                    <?php foreach ($news_2 as $key => $news_2) : ?>
                                        <li><a href="Blog-detail?id=<?php echo $news_2['id']; ?>"><span class="title">Electronics</span><span class="count">&nbsp;8626</span></a>
                                        </li>
                                    <?php endforeach; ?>
                                </ul>
                            </div>

                        </div>
                    </aside>
                </div>
                <!--page-sidebar-->

                <?php else: ?>
                    <div class="col-sm-12 text-center">
                        <h1>ยังไม่มีข้อมูล ข่าวสาร</h1>
                    </div>
                <?php endif; ?>

            </div>
        </div>

    </div>

</div>
