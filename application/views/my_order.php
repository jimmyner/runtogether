<?php $profile = $this->db->get_where('tbl_member', ['email' => $this->session->userdata('email')])->row_array(); ?>
<?php $type = $this->db->get('tbl_type')->result_array(); ?>
<div class="main-container">
    <div class="container">
        <div class="row">
            <div class="col-md-3 page-sidebar">
                <aside>
                    <div class="inner-box">
                        <div class="user-panel-sidebar">
                            <div class="collapse-box">
                                <h5 class="collapse-title no-border"> โปรไฟล์ของฉัน <a href="#MyClassified" aria-expanded="true" data-toggle="collapse" class="pull-right"><i class="fa fa-angle-down"></i></a></h5>

                                <div class="panel-collapse collapse show" id="MyClassified">
                                    <ul class="acc-list">
                                        <li><a href="My-profile"><i class="icon-home"></i>
                                                โปรไฟล์ของฉัน </a></li>
                                    </ul>
                                </div>
                            </div>
                            <!-- /.collapse-box  -->
                            <div class="collapse-box">
                                <h5 class="collapse-title"> เครื่องมือต่างๆ <a href="#MyAds" aria-expanded="true" data-toggle="collapse" class="pull-right"><i class="fa fa-angle-down"></i></a></h5>

                                <div class="panel-collapse collapse show" id="MyAds">
                                    <ul class="acc-list">
                                        <li><a href="my-activity"><i class="icon-docs"></i> ลงทะเบียนงานวิ่ง </a></li>
                                        <li><a class="active" href="my-order"><i class="icon-heart"></i> ประวัติงานวิ่ง </a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.inner-box  -->

                </aside>
            </div>
            <!--/.page-sidebar-->

            <div class="col-md-9 page-content">
                <div class="inner-box">
                    <h2 class="title-2"><i class="icon-docs"></i> ประวัติงานวิ่ง </h2>

                    <div class="table-responsive">
                        <div class="table-action">


                            <div class="table-search pull-right col-sm-7">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-sm-5 control-label text-right">Search <br>
                                            <a title="clear filter" class="clear-filter" href="#clear">[clear]</a>
                                        </label>

                                        <div class="col-sm-7 searchpan">
                                            <input type="text" class="form-control" id="filter">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <table id="addManageTable" class="table table-striped table-bordered add-manage-table table demo" data-filter="#filter" data-filter-text-only="true">
                            <thead>
                                <tr>
                                    <th> รูปภาพ</th>
                                    <th data-sort-ignore="true"> หัวข้องานวิ่ง</th>
                                    <th>สถานะ</th>
                                    <th data-type="numeric"> ราคา</th>
                                    <th> ตัวเลือก</th>
                                </tr>
                            </thead>
                            <tbody>

                                <?php foreach ($order as $key => $order) : ?>
                                    <tr>
                                        <td style="width:14%" class="add-img-td">
                                            <img class="thumbnail  img-responsive" src="uploads/event/<?php echo $order->file_cover; ?>" alt="img">
                                        </td>

                                        <td style="width:58%" class="ads-details-td">
                                            <div>
                                                <p><strong> <?php echo $order->name_event; ?></strong></p>
                                            </div>
                                        </td>
                                        <td class="action-td">
                                            <?php if ($order->status == 0) : ?>
                                                <p><a href="my-payment?id=<?php echo  $order->idevent; ?> " class="btn btn-danger btn-sm"> <i class="fa fa-mail-forward"></i> ชำระเงิน </a></p>
                                            <?php elseif ($order->status == 1) : ?>
                                                <p><button class="btn btn-warning btn-sm"> <i class="fa fa-mail-forward"></i> กำลังดำเนินการ </button></p>
                                            <?php elseif ($order->status == 2) : ?>
                                                <p><button class="btn btn-success btn-sm"> <i class="fa fa-mail-forward"></i> เสร็จสิ้น </button></p>
                                            <?php else : ?>
                                                <p><a href="my-payment?id=<?php echo  $order->idevent; ?> " class="btn btn-danger btn-sm"> <i class="fa fa-mail-forward"></i> ล้มเหลว </a></p>
                                            <?php endif; ?>
                                        </td>
                                        <td style="width:16%" class="price-td">
                                            <div><strong> <?php echo $order->price; ?> บาท</strong></div>
                                        </td>

                                        <td style="width:10%" class="action-td">
                                            <div>
                                                <p><a href="my-activity-view?id=<?php echo $order->idevent; ?>" class="btn btn-primary btn-sm"> <i class="fa fa-edit"></i> รายละเอียด </a></p>
                                                <p><a href="my-register-view?id=<?php echo $order->idevent ;?>" class="btn btn-info btn-sm"> <i class="fa fa-user-circle"></i> ข้อมูลผู้สมัคร </a></p>
                                            </div>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>


                            </tbody>
                        </table>
                    </div>
                    <!--/.row-box End-->

                </div>
            </div>

            <!--/.page-content-->
        </div>
        <!--/.row-->
    </div>
    <!--/.container-->
</div>
<!-- /.main-container -->