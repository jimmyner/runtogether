<?php $id = $this->input->get('id'); ?>
<?php $detail = $this->db->get_where('tbl_blog', ['id' => $id])->row_array(); ?>

<div class="intro-inner">
    <div class="about-intro" style="
    background:url(images/bg2.jpg) no-repeat center;
	background-size:cover;">
        <div class="dtable hw100">
            <div class="dtable-cell hw100">
                <div class="container text-center animated fadeInDown">

                    <h1 class="intro-title">Classified Blogs</h1>

                    <h2>Sign up to subscribe to email alerts and <br> you'll never miss a post. Subscribe Now !</h2>

                </div>
            </div>
        </div>
    </div>
    <!--/.about-intro -->

</div>
<!-- /.intro-inner -->

<div class="main-container inner-page">
    <div class="container">
        <div class="section-content">
            <div class="row ">
                <div class="col-sm-8 blogLeft">
                    <div class="blog-post-wrapper">


                        <article class="blog-post-item">
                            <div class="inner-box">


                                <!--blog image-->
                                <div class="blog-post-img">

                                    <a href="blog-details.html">
                                        <figure>
                                            <img class="img-responsive" alt="blog-post image" src="uploads/blog/<?php echo $detail['file_name']; ?>">
                                        </figure>
                                    </a>
                                </div>

                                <!--blog content-->

                                <div class="blog-post-content-desc">


                                    <span class="info-row blog-post-meta"> <span class="date"><i class=" icon-clock"> </i> <?php echo $detail['create_at']; ?> </span> -
                                        <span class="author"> <i class="fa fa-user"></i> <a href="#" title="Posts by Jhon Doe" rel="author">Admin</a> </span> </span>


                                    <div class="blog-post-content">
                                        <h2><a><?php echo $detail['name']; ?></a></h2>


                                        <div class="blog-article-text">
                                            <?php echo $detail['details']; ?>
                                        </div>


                                    </div>


                                    <div class="clearfix">
                                        <div class="col-md-12  blog-post-bottom">

                                            <ul class="share-this-post">
                                                <li>Share This:</li>

                                                <li><a class="google-plus"><i class="fab fa-google-plus"></i>Google-plus</a>
                                                </li>
                                                <li><a class="facebook"><i class="fab fa-facebook"></i>Facebook</a>
                                                </li>
                                                <li><a><i class="fab fa-twitter"></i>Twitter</a>
                                                </li>
                                                <li><a class="pinterest"><i class="fab fa-pinterest"></i>Pinterest</a>
                                                </li>

                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </article>


                    </div>
                    <!--/.blog-post-wrapper-->
                </div>
                <!--blogLeft-->


                <div class="col-sm-4 blogRight page-sidebar">
                    <aside>
                        <div class="inner-box">

                            <!--/.categories-list-->
                            <div class="categories-list  list-filter">
                                <h5 class="list-title uppercase"><strong><a href="#"> recent
                                            popular</a></strong></h5>


                                <div class="blog-popular-content">
                                    <div class="item-list">
                                        <div class="row">

                                            <div class="col-sm-4 col-xs-4 no-padding photobox">
                                                <div class="add-image"><a href="ads-details.html"><img class="no-margin" src="images/blog/5.jpg" alt="img"></a>
                                                </div>
                                            </div>
                                            <!--/.photobox-->
                                            <div class="col-sm-8 col-xs-8 add-desc-box">
                                                <div class="ads-details">
                                                    <h5 class="add-title"><a href="ads-details.html">Sed aliquam leo et
                                                            dui venenatis cursus</a></h5>
                                                    <span class="info-row"> <span class="date"><i class=" icon-clock"> </i> Today 1:21 pm </span> </span>
                                                </div>
                                            </div>
                                            <!--/.add-desc-box-->
                                        </div>

                                    </div>


                                </div>


                                <div style="clear:both"></div>

                                <!--/.categories-list-->

                            </div>

                        </div>
                    </aside>
                </div>
                <!--page-sidebar-->

            </div>
        </div>

    </div>
</div>
<!-- /.main-container -->