    <?php $event = $this->db->get_where('tbl_event', ['status' => '2'], 6)->result_array();  ?>
    <?php $data = date('Y-m-d') ;?>
    <?php
    $month = array(
        '01'  => 'มกราคม', '02'  => 'กุมภาพันธ์', '03'  => 'มีนาคม',
        '04'  => 'เมษายน', '05'  => 'พฤษภาคม', '06'  => 'มิถุนายน',
        '07'  => 'กรกฎาคม', '08'  => 'สิงหาคม', '09'  => 'กันยายน',
        '10'  => 'ตุลาคม', '11'  => 'พฤศจิกายน', '12'  => 'ธันวาคม',
    );
    function thaiDate($datetime)
    {
        list($date) = explode(' ', $datetime); // แยกวันที่ กับ เวลาออกจากกัน
        list($Y, $m, $d) = explode('-', $date); // แยกวันเป็น ปี เดือน วัน
        $Y = $Y; // เปลี่ยน ค.ศ. เป็น พ.ศ.
        switch ($m) {
            case "01":
                $m = "มกราคม";
                break;
            case "02":
                $m = "กุมภาพันธ์";
                break;
            case "03":
                $m = "มีนาคม";
                break;
            case "04":
                $m = "เมษายน";
                break;
            case "05":
                $m = "พฤษภาคม";
                break;
            case "06":
                $m = "มิถุนายน";
                break;
            case "07":
                $m = "กรกฎาคม";
                break;
            case "08":
                $m = "สิงหาคม";
                break;
            case "09":
                $m = "กันยายน";
                break;
            case "10":
                $m = "ตุลาคม";
                break;
            case "11":
                $m = "พฤศจิกายน ";
                break;
            case "12":
                $m = "ธันวาคม";
                break;
        }
        return $d . " " . $m . " " . $Y;
    }
    ?>
    <div class="intro-modern">
        <div class="inner">
            <div class="container text-center">
                <h1 class="title-6 animated fadeInDown"> ระบบสมัครงานวิ่งอันดับ 1 ของไทย
                    สนับสนุนงานวิจัยโดย สกสว.</h1>
            </div>
        </div>

        <div class="layer-bg">

        </div>

        <div class="bg-slider-wrapper">
            <div class="bg-slider">
                <div class="bg-item" style="background-image: url(uploads/04b3f6aa-77bb-43bf-bdfe-ec89ded8ea4a.png);"></div>
            </div>
        </div>
    </div>
    <!-- /.intro -->


    <section class="event-listing section-ev">
        <div class="container">
            <?php if (!empty($event)) : ?>
                <div class="section-header listing-title-holder">
                    <div class="row for-list align-center">
                        <div class="event-title-holder  col-lg-4 col-md-3 col-sm-12 mr-auto">
                            <h2 class="title text-left"> รวมงานวิ่ง </h2>
                        </div>
                    </div>
                </div>


                <div class="row">
                    <?php foreach ($event as $key => $event) : ?>
                        <?php $member = $this->db->get_where('tbl_member', ['id' => $event['member_id']])->row_array();  ?>
                        <div class="col-md-4 col-sm-6 col-12 event-item-col">
                            <div class="card card-event info-overlay">
                                <div class="img has-background" style="background-image: url(uploads/event/<?php echo $event['file_cover']; ?>); background-size:cover ">
                                    <div class="pop-info ">
                                        <span class="event-badges ">
                                            <?php if ($data < $event['time_out']) : ?>
                                                <span class="badge price-tag big badge-default" style="background:#52c41a;color:#fff;"> เปิดรับสมัคร</span>
                                            <?php else : ?>
                                                <span class="badge price-tag big badge-default" style="background:#868686;color:#fff;"> ปิดรับสมัคร</span>
                                            <?php endif; ?>
                                        </span>
                                    </div>

                                    <a href="event-details?id=<?php echo $event['id']; ?>">
                                        <img alt="340x230" class="card-img-top img-responsive" data-holder-rendered="true" src="images/10x6.gif">
                                    </a>
                                </div>
                                <div class="card-body">
                                    <h4 class="card-title">
                                        <a href="event-details?id=<?php echo $event['id']; ?>"><?php echo $event['name_event']; ?></a>
                                    </h4>
                                    <p class="card-text hide">Donec imperdiet leo ac ipsum blandit auctor.</p>

                                    <div class="card-event-info">
                                        <p class="event-location"><i class="fa icon-location-1" style="color:red"></i>
                                            <a class="location" href=""> เวลาปิดรับสมัคร <?php echo thaiDate($event['time_out']); ?></a></p>
                                        <p class="event-time"><i class="fa icon-calendar-1" style="color:red"></i> วันที่สร้างโพส <?php echo thaiDate($event['create_at']); ?> </p>
                                    </div>
                                </div>
                                <div class="card-footer">
                                    <div class="pull-left left">
                                        <div class="">by <b style="color:#000;"><?php echo $member['first_name'] . ' ' . $member['last_name']; ?></b></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>

            <?php else : ?>
                <div class="section-header listing-title-holder">
                    <div class="row for-list align-center">
                        <div class="event-title-holder  col-lg-12 col-md-12 col-sm-12 mr-auto">
                            <h2 class="title text-center"> ไม่มีข้อมูลงานวิ่ง </h2>
                        </div>
                    </div>
                </div>
            <?php endif; ?>

        </div>

    </section>


    <div class="page-bottom-info">
        <div class="page-bottom-info-inner">

            <div class="page-bottom-info-content text-center">
                <h1>สามารถติดต่อสอบถามข้อมูลเพิ่มเติม หรือ โทร (000) 555-5556</h1>
                <a class="btn  btn-lg btn-primary-dark" href="tel:+000000000">
                    <i class="icon-mobile"></i> <span class="hide-xs color50">Call Now:</span> (000) 555-5555 </a>
            </div>

        </div>
    </div>