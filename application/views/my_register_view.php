<?php $id_event = $this->input->get('id'); ?>
<?php $get_register_view = $this->db->get_where('tbl_register_run', ['id_event' => $id_event])->result_array(); ?>

<?php $this->db->where('id_event',$id_event); ?>
<?php $count = $this->db->count_all_results('tbl_register_run'); ?>

<?php
$month = array(
    '01'  => 'มกราคม', '02'  => 'กุมภาพันธ์', '03'  => 'มีนาคม',
    '04'  => 'เมษายน', '05'  => 'พฤษภาคม', '06'  => 'มิถุนายน',
    '07'  => 'กรกฎาคม', '08'  => 'สิงหาคม', '09'  => 'กันยายน',
    '10'  => 'ตุลาคม', '11'  => 'พฤศจิกายน', '12'  => 'ธันวาคม',
);
function thaiDate($datetime)
{
    list($date, $time) = explode(' ', $datetime); // แยกวันที่ กับ เวลาออกจากกัน
    list($H, $i, $s) = explode(':', $time); // แยกเวลา ออกเป็น ชั่วโมง นาที วินาที
    list($Y, $m, $d) = explode('-', $date); // แยกวันเป็น ปี เดือน วัน
    $Y = $Y + 543; // เปลี่ยน ค.ศ. เป็น พ.ศ.
    switch ($m) {
        case "01":
            $m = "ม.ค.";
            break;
        case "02":
            $m = "ก.พ.";
            break;
        case "03":
            $m = "มี.ค.";
            break;
        case "04":
            $m = "เม.ย.";
            break;
        case "05":
            $m = "พ.ค.";
            break;
        case "06":
            $m = "มิ.ย.";
            break;
        case "07":
            $m = "ก.ค.";
            break;
        case "08":
            $m = "ส.ค.";
            break;
        case "09":
            $m = "ก.ย.";
            break;
        case "10":
            $m = "ต.ค.";
            break;
        case "11":
            $m = "พ.ย.";
            break;
        case "12":
            $m = "ธ.ค.";
            break;
    }
    return $d . " " . $m . " " . $Y;
}
?>

<div class="main-container">
    <div class="container">
        <div class="row">
            <div class="col-md-3 page-sidebar">
                <aside>
                    <div class="inner-box">
                        <div class="user-panel-sidebar">
                            <div class="collapse-box">
                                <h5 class="collapse-title no-border"> โปรไฟล์ของฉัน <a href="#MyClassified" aria-expanded="true" data-toggle="collapse" class="pull-right"><i class="fa fa-angle-down"></i></a></h5>

                                <div class="panel-collapse collapse show" id="MyClassified">
                                    <ul class="acc-list">
                                        <li><a href="My-profile"><i class="icon-home"></i>
                                                โปรไฟล์ของฉัน </a></li>

                                    </ul>
                                </div>
                            </div>
                            <!-- /.collapse-box  -->
                            <div class="collapse-box">
                                <h5 class="collapse-title"> เครื่องมือต่างๆ <a href="#MyAds" aria-expanded="true" data-toggle="collapse" class="pull-right"><i class="fa fa-angle-down"></i></a></h5>

                                <div class="panel-collapse collapse show" id="MyAds">
                                    <ul class="acc-list">
                                        <li><a href="my-activity"><i class="icon-docs"></i> ลงทะเบียนงานวิ่ง </a></li>
                                        <li><a class="active" href="my-order"><i class="icon-heart"></i> ประวัติงานวิ่ง </a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.inner-box  -->

                </aside>
            </div>
            <!--/.page-sidebar-->

            <div class="col-md-9 page-content">
                <div class="inner-box">
                    <div class="row">
                        <div class="col-md-5">
                            <h2 class="title-2"><i class="icon-docs"></i> ข้อมูลผู้สมัคร </h2>
                        </div>
                        <div class="col-md-7 text-right" >
                            <div class="hdata">
                                <div class="mcol-left">
                                    <!-- Icon with blue background -->
                                    <i class="fa fa-user ln-shadow"></i></div>
                                <div class="mcol-right">
                                    <!-- Number of visitors -->
                                    <p><a href="#"> <?php echo $count; ?> </a> <em>จำนวน </em></p>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>

                    <div class="table-responsive">
                        <div class="table-action">


                            <div class="table-search pull-right col-sm-7">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-sm-5 control-label text-right">Search <br>
                                            <a title="clear filter" class="clear-filter" href="#clear">[clear]</a>
                                        </label>

                                        <div class="col-sm-7 searchpan">
                                            <input type="text" class="form-control" id="filter">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php
                        $i = 1;
                        $e = 1;
                        ?>
                        <table id="addManageTable" class="table table-striped table-bordered add-manage-table table demo" data-filter="#filter" data-filter-text-only="true">
                            <thead>
                                <tr>
                                    <th style="text-align:center"> ชื่อ - นามสกุล</th>
                                    <th style="text-align:center"> อายุ</th>
                                    <th style="text-align:center"> เลขบัตรประชาชน</th>
                                    <th style="text-align:center"> เบอร์โทร</th>
                                    <th style="text-align:center"> สถานะ</th>
                                    <th style="text-align:center"> สมัครวันที่</th>
                                    <!-- <th> ตัวเลือก</th> -->
                                </tr>
                            </thead>
                            <tbody style="text-align:center">
                                <?php foreach ($get_register_view as $Key => $data) { ?>
                                    <tr>
                                        <td>
                                            <div>
                                                <p><strong> <?php echo $data['full_name']; ?></strong></p>
                                            </div>
                                        </td>
                                        <td>
                                            <div>
                                                <p><strong> <?php echo $data['age']; ?></strong></p>
                                            </div>
                                        </td>
                                        <td>
                                            <div>
                                                <p><strong> <?php echo $data['id_card']; ?></strong></p>
                                            </div>
                                        </td>
                                        <td>
                                            <div>
                                                <p><strong> <?php echo $data['tel']; ?></strong></p>
                                            </div>
                                        </td>
                                        <td>
                                            <div>
                                                <?php if ($data['status'] == 1) { ?>
                                                    <p><strong> <a href="#exampleModal3<?php echo $i++; ?>" data-toggle="modal"><span class="badge badge-warning" style="color:#fff;"><u>คลิ๊กเพื่อตรวจสอบหลักฐาน</u></span></a> </strong></p>
                                                <?php } elseif ($data['status'] == 2) { ?>
                                                    <p><strong> <span class="badge badge-success <?php echo $i++; ?>" style="color:#fff;">อนุมัติเรียบร้อยแล้ว</span> </strong></p>
                                                <?php } else { ?>
                                                    <p><strong> <span class="badge badge-danger <?php echo $i++; ?>" style="color:#fff;">ไม่ผ่านการอนุมัติ</span> </strong></p>
                                                <?php } ?>
                                            </div>
                                        </td>
                                        <td>
                                            <div>
                                                <p><strong> <?php echo thaiDate($data['create_at']); ?></strong></p>
                                            </div>
                                        </td>
                                    </tr>
                                    <!-- Modal -->
                                    <div class="modal fade" id="exampleModal3<?php echo $e++; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="exampleModalLabel">หลักฐานการโอนเงิน <?php echo $data['id']; ?></h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <img src="uploads/payment/<?php echo $data['file_name']; ?>" alt="">
                                                </div>
                                                <div class="modal-footer">
                                                    <a href="my-register-status?id=<?php echo $data['id']; ?>&status=<?php echo 2; ?>&id_event=<?php echo $id_event; ?>" OnClick="return confirm('คุณต้องการอนุมัติการสมัคร ใช่หรือไม่ ??');" class="btn btn-primary">อนุมัติ</a>
                                                    <a href="my-register-status?id=<?php echo $data['id']; ?>&status=<?php echo 0; ?>&id_event=<?php echo $id_event; ?>" OnClick="return confirm('คุณต้องการไม่อนุมัติการสมัคร ใช่หรือไม่ ??');" class="btn btn-danger">ไม่อนุมัติ</a>
                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">ปิด</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                    <!--/.row-box End-->

                </div>
            </div>

            <!--/.page-content-->
        </div>
        <!--/.row-->
    </div>
    <!--/.container-->
</div>
<!-- /.main-container -->