<?php $profile  = $this->db->get_where('tbl_member', ['email' => $this->session->userdata('email')])->row_array(); ?>
<?php $id       = $this->input->get('id'); ?>
<div class="main-container">
    <div class="container">
        <div class="row">
            <div class="col-md-3 page-sidebar">
                <aside>
                    <div class="inner-box">
                        <div class="user-panel-sidebar">
                            <div class="collapse-box">
                                <h5 class="collapse-title no-border"> โปรไฟล์ของฉัน <a href="#MyClassified" aria-expanded="true" data-toggle="collapse" class="pull-right"><i class="fa fa-angle-down"></i></a></h5>

                                <div class="panel-collapse collapse show" id="MyClassified">
                                    <ul class="acc-list">
                                        <li><a href="My-profile"><i class="icon-home"></i>
                                            โปรไฟล์ของฉัน </a></li>

                                    </ul>
                                </div>
                            </div>
                            <!-- /.collapse-box  -->
                            <div class="collapse-box">
                                <h5 class="collapse-title"> เครื่องมือต่างๆ <a href="#MyAds" aria-expanded="true" data-toggle="collapse" class="pull-right"><i class="fa fa-angle-down"></i></a></h5>

                                <div class="panel-collapse collapse show" id="MyAds">
                                    <ul class="acc-list">
                                        <li><a  href="my-activity"><i class="icon-docs"></i> ลงทะเบียนงานวิ่ง </a></li>
                                        <li><a class="active" href="my-order"><i class="icon-heart"></i> ประวัติงานวิ่ง <span class="badge badge-secondary">42</span> </a></li>
                                    </ul>
                                </div>
                            </div>
                            <!-- /.collapse-box  -->

                            <!-- <div class="collapse-box">
                                <h5 class="collapse-title"> จุดชำระเงิน <a href="#TerminateAccount" aria-expanded="true" data-toggle="collapse" class="pull-right"><i class="fa fa-angle-down"></i></a></h5>
                                <div class="panel-collapse collapse show" id="TerminateAccount">
                                    <ul class="acc-list">
                                        <li><a href="my-payment"><i class="icon-cancel-circled "></i> ชำระเงินค่าลงงาน </a></li>
                                    </ul>
                                </div>
                            </div> -->
                            
                            <!-- /.collapse-box  -->
                        </div>
                    </div>
                    <!-- /.inner-box  -->

                </aside>
            </div>
            <!--/.page-sidebar-->

            <div class="col-md-9 page-content">
                <div class="inner-box">
                    <div class="row">
                        <div class="col-md-5 col-xs-4 col-xxs-12">

                        </div>
                        <div class="col-md-7 col-xs-8 col-xxs-12">
                            <div class="header-data text-center-xs">
                                <div class="hdata">
                                    <div class="mcol-left">
                                        <!-- Icon with red background -->
                                        <i class="fas fa-envelope ln-shadow"></i></div>
                                    <div class="mcol-right">
                                        <!-- Number of visitors -->
                                        <p><a href="account-message-inbox.html">15</a> <em>Mail</em></p>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>

                                <!-- Traffic data -->
                                <div class="hdata">
                                    <div class="mcol-left">
                                        <!-- Icon with red background -->
                                        <i class="fa fa-eye ln-shadow"></i></div>
                                    <div class="mcol-right">
                                        <!-- Number of visitors -->
                                        <p><a href="#">7000</a> <em>visits</em></p>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>

                                <!-- revenue data -->
                                <div class="hdata">
                                    <div class="mcol-left">
                                        <!-- Icon with green background -->
                                        <i class="icon-th-thumb ln-shadow"></i></div>
                                    <div class="mcol-right">
                                        <!-- Number of visitors -->
                                        <p><a href="#">12</a><em>Ads</em></p>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>

                                <!-- revenue data -->
                                <div class="hdata">
                                    <div class="mcol-left">
                                        <!-- Icon with blue background -->
                                        <i class="fa fa-user ln-shadow"></i></div>
                                    <div class="mcol-right">
                                        <!-- Number of visitors -->
                                        <p><a href="#">18</a> <em>Favorites </em></p>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="inner-box">
                    <div class="welcome-msg">
                        <h3 class="page-sub-header2 clearfix no-padding"><?php echo $profile['email']; ?> </h3>
                        <span class="page-sub-header-sub small"><?php echo (empty($profile['update_at']) ? 'ยังไม่มีข้อมูลอัพเดท' : 'คุณอัพเดทข้อมูลล่าสุดเมื่อ' . $profile['update_at']); ?></span>
                    </div>
                    <div id="accordion" class="panel-group">
                        <div class="card card-default">
                            <div class="card-header">
                                <h4 class="card-title"><a href="#collapseB1" aria-expanded="true" data-toggle="collapse"> My
                                        details </a></h4>
                            </div>
                            <div class="panel-collapse collapse show" id="collapseB1">
                                <div class="card-body">
                                    <form action="my-payment-create" class="form-horizontal" role="form" method="POST" enctype="multipart/form-data">
                                    <input type="hidden" name="member_id" value="<?php echo $profile['id']; ?>">
                                    <input type="hidden" name="event_id"  value="<?php echo $id; ?>">
                                        <div class="form-group" style="margin-left: 13px;">
                                            <div class="row">
                                                <div class="col-sm-9">
                                                    <label class="control-label">โอนเงินผ่านบัญชีธนาคาร</label><br>
                                                    <img src="uploads/bank_logo.gif" alt="" style="width:200px"> 
                                                    <br>
                                                    
                                                    <p>ชำระเงินโดยโอนไปยังบัญชีใดบัญชีหนึ่งดังต่อไปนี้: </p>
                                                    <p>ชื่อบัญชี:	Kanokon Chattukul </p>
                                                    <p>บัญชี:	ออมทรัพย์</p> 
                                                    <p> - ธนาคาร กรุงเทพ เลขที่บัญชี 943-0-03956-1</p>
                                                    <p> - ธนาคาร ไทยพาณิชย์ เลขที่บัญชี 418-021103-8</p>
                                                    <p> - ธนาคาร กสิกรไทย เลขที่บัญชี 050-2-85390-2</p>
                                                    <p> - ธนาคาร กรุงไทย เลขที่บัญชี 985-5-08578-7 </p>
                                                </div>
                                            </div>
                                        </div>
                                  
                                        <div class="form-group" style="margin-left: 13px;">
                                            <div class="row">
                                                <div class="col-sm-4">
                                                    <label class="control-label">แนบสลิปใบเสร็จ</label>
                                                    <input type="file" class="form-control" name="file_name" value="" required>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group" style="margin-left: 13px;">
                                            <div class="row">
                                                <div class="col-sm-4">
                                                <label class="control-label">วัน</label>
                                                    <input type="date" class="form-control" name="date" value="" required>
                                                </div>
                                                <div class="col-sm-4">
                                                <label class="control-label">เวลา</label>
                                                    <input type="time" class="form-control" name="time" value="" required>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">เลขที่อ้างอิง</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" name="number" value="" required>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">รายละเอียดเพิ่มเติม</label>
                                            <div class="col-sm-9">
                                                <textarea class="form-control" id="message" name="detail" placeholder="" rows="7"></textarea>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-sm-offset-3 col-sm-9"></div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-sm-offset-3 col-sm-9">
                                                <button type="submit" class="btn btn-default">บันทึก</button>
                                            </div>
                                        </div>

                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--/.row-box End-->

                </div>
            </div>
            <!--/.page-content-->
        </div>
        <!--/.row-->
    </div>
    <!--/.container-->
</div>
<!-- /.main-container -->