

<div class="main-container">
        <div class="container">
            <div class="row">
                <div class="col-md-12 page-content">
                    <div class="inner-box">
                        <h2 class="title-2"><i class="icon-money"></i> ประวัติการสมัครงานวิ่ง </h2>

                        <div style="clear:both"></div>

                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th><span> #</span></th>
                                    <th>รูปงานวิ่ง</th>
                                    <th><strong>ชื่องานวิ่ง</strong>รู</strong></th>
                                    <th> ประเภทงานวิ่ง</th>
                                    <th> ขนาดเสื้อ</th>
                                    <th> บัตรประชาชน</th>
                                    <th> ชื่อ-นามสกุล</th>
                                    <th> เวลา</th>
                                    <th> สถานะ</th>
                                </tr>
                                </thead>
                                <tbody>

                                <?php foreach ($passpost as $key => $value) :?>
                                <tr>
                                    <td>#01PA</td>
                                    <td><img src="uploads/event/<?php echo $value['file_cover']; ?>" alt="" style="width: 150px;"></td>
                                    <td><?php echo $value['name_event'] ?></td>
                                    <td><?php echo $value['name_marathon'].' '.$value['length'].' '.$value['price'] ; ?> </td>
                                    <td>$40</td>
                                    <td><?php echo $value['id_card']; ?></td>
                                    <td><?php echo $value['full_name']; ?></td>
                                    <td><?php echo $value['create_at']; ?></td>
                                    <td>
                                        <?php if ($value['status_run'] == 0) : ?>
                                        <span class="badge badge-danger"> ชำระเงินไม่ถูกต้อง </span> 
                                        <?php elseif($value['status_run'] == 1) : ?>
                                        <span class="badge badge-warning"> กำลังดำเนินการ </span> 
                                        <?php else : ?>
                                        <span class="badge badge-success"> เสร็จสิน </span> 
                                        <?php endif; ?>
                                    </td>
                                </tr>
                                <?php endforeach; ?>
      
                                </tbody>
                            </table>
                        </div>

                        <div style="clear:both"></div>

                    </div>
                </div>
                <!--/.page-content-->
            </div>
            <!--/.row-->
        </div>
        <!--/.container-->
    </div>
    <!-- /.main-container -->