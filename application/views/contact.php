
    <!-- /.intro-inner -->

    <div class="main-container">
        <div class="container">
            <div class="row clearfix">
                <div class="col-md-4">
                    <div class="contact_info">
                        <h5 class="list-title gray"><strong>Contact us</strong></h5>

                        <div class="contact-info ">
                            <div class="address">
                                <p class="p1">220 Fifth Ave</p>

                                <p class="p1">2nd Flr. New York, NY 10001 </p>

                                <p>Email: info@@demos.org</p>

                                <p>Toll Free: 212-633-1405</p>

                                <p>&nbsp;</p>

                                <div>

                                    <p><strong><a href="#">Get a Quote</a></strong></p>

                                    <p><strong> <a href="login.html">Client Area Login</a></strong></p>

                                    <p><strong> <a href="#skypeid" class="skype">Live Chat</a></strong></p>

                                    <p><strong> <a href="faq.html">Knowledge Base</a></strong></p>

                                </div>
                            </div>
                        </div>

                        <div class="social-list"><a target="_blank" href="https://twitter.com/"><i
                                class="fa fa-twitter fa-lg "></i></a>
                            <a target="_blank" href="https://www.facebook.com/"><i
                                    class="fa fa-facebook fa-lg "></i></a>
                            <a target="_blank" href="https://plus.google.com"><i
                                    class="fa fa-google-plus fa-lg "></i></a>
                            <a target="_blank" href="https://www.pinterest.com/"><i class="fa fa-pinterest fa-lg "></i></a>
                        </div>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="contact-form">
                        <h5 class="list-title gray"><strong>คิดต่อเรา</strong></h5>


                        <form class="form-horizontal" action="ContacMe" method="post">
                            <fieldset>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                                <input id="firstname" name="f_name" type="text" placeholder="ชื่อ"
                                                       class="form-control">
                                        </div>
                                    </div>

                                    <div class="col-sm-6">
                                        <div class="form-group">
                                                <input id="lastname" name="l_name" type="text" placeholder="นามสกุล"
                                                       class="form-control">
                                        </div>
                                    </div>

                                    <div class="col-sm-6">
                                        <div class="form-group">
                                                <input id="tel" name="tel" type="text"
                                                       placeholder="เบอร์โทร" class="form-control">
                                        </div>
                                    </div>

                                    <div class="col-sm-6">
                                        <div class="form-group">
                                                <input id="email" name="email" type="text" placeholder="อีเมล์"
                                                       class="form-control">
                                        </div>
                                    </div>

                                    <div class="col-xl-12">
                                        <div class="form-group">
                                                <textarea class="form-control" id="message" name="detail" placeholder="" rows="7"></textarea>
                                        </div>
                                        <div class="form-group">
                                                <button type="submit" class="btn btn-primary btn-lg">ส่ง</button>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /.main-container -->

