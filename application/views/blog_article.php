<?php $article = $this->db->get_where('tbl_blog', ['type' => 1])->result_array(); ?>
<?php $article_2 = $this->db->get_where('tbl_blog', ['type' => 1])->result_array(); ?>

<div class="intro-inner">
    <div class="about-intro" style="
    background:url(images/bg2.jpg) no-repeat center;
	background-size:cover;">
        <div class="dtable hw100">
            <div class="dtable-cell hw100">
                <div class="container text-center animated fadeInDown">
                    <h1 class="intro-title"> บทความ</h1>
                    <!-- <h2>Sign up to subscribe to email alerts and <br> you'll never miss a post. Subscribe Now !</h2> -->
                </div>
            </div>
        </div>
    </div>
    <!--/.about-intro -->
</div>
<!-- /.intro-inner -->

<div class="main-container inner-page">
    <div class="container">
        <div class="section-content">
            <div class="row ">
                <?php if (!empty($article)) : ?>
                    <div class="col-sm-8 blogLeft">
                        <div class="blog-post-wrapper">

                            <?php foreach ($article as $key => $article) : ?>
                                <article class="blog-post-item">
                                    <div class="inner-box">
                                        <!--blog image-->
                                        <div class="blog-post-img">

                                            <a href="Blog-detail?id=<?php echo $article['id']; ?>">
                                                <figure>
                                                    <img class="img-responsive" alt="blog-post image" src="uploads/blog/<?php echo $article['file_name']; ?>">
                                                </figure>
                                            </a>
                                        </div>
                                        <!--blog content-->
                                        <div class="blog-post-content-desc">

                                            <span class="info-row blog-post-meta"> <span class="date"><i class=" icon-clock"> </i> <?php echo $article['create_at']; ?> </span> -
                                                <span class="author"> <i class="fa fa-user"></i> <a rel="author" title="Posts by Jhon Doe" href="#"> Admin </a> </span> </span>
                                            <div class="blog-post-content">
                                                <h2><a href="Blog-detail?id=<?php echo $article['id']; ?>"><?php echo $article['name']; ?></a></h2>

                                                <div class="row">
                                                    <div class="col-md-12 clearfix blog-post-bottom">
                                                        <a class="btn btn-primary  pull-left" href="Blog-detail?id=<?php echo $article['id']; ?>">อ่านต่อ..</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </article>
                            <?php endforeach; ?>


                        </div>
                        <!--/.blog-post-wrapper-->
                    </div>
                    <!--blogLeft-->


                   
                    <!--page-sidebar-->
                <?php else : ?>
                    <div class="col-sm-12 text-center">
                        <h1>ยังไม่มีข้อมูล บทความ</h1>
                    </div>
                <?php endif; ?>

            </div>
        </div>

    </div>

</div>