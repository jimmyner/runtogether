<?php $profile = $this->db->get_where('tbl_member', ['email' => $this->session->userdata('email')])->row_array(); ?>
<div class="main-container">
    <div class="container">
        <div class="row">
            <div class="col-md-3 page-sidebar">
                <aside>
                    <div class="inner-box">
                        <div class="user-panel-sidebar">
                            <div class="collapse-box">
                                <h5 class="collapse-title no-border"> โปรไฟล์ของฉัน <a href="#MyClassified" aria-expanded="true" data-toggle="collapse" class="pull-right"><i class="fa fa-angle-down"></i></a></h5>

                                <div class="panel-collapse collapse show" id="MyClassified">
                                    <ul class="acc-list">
                                        <li><a  class="active" href="My-profile"><i class="icon-home"></i>
                                            โปรไฟล์ของฉัน </a></li>

                                    </ul>
                                </div>
                            </div>
                            <!-- /.collapse-box  -->
                            <div class="collapse-box">
                                <h5 class="collapse-title"> เครื่องมือต่างๆ <a href="#MyAds" aria-expanded="true" data-toggle="collapse" class="pull-right"><i class="fa fa-angle-down"></i></a></h5>

                                <div class="panel-collapse collapse show" id="MyAds">
                                    <ul class="acc-list">
                                        <li><a  href="my-activity"><i class="icon-docs"></i> ลงทะเบียนงานวิ่ง </a></li>
                                        <li><a href="my-order"><i class="icon-heart"></i> ประวัติงานวิ่ง </a></li>
                                    </ul>
                                </div>
                            </div>
                            <!-- /.collapse-box  -->

                            <!-- <div class="collapse-box">
                                <h5 class="collapse-title"> จุดชำระเงิน <a href="#TerminateAccount" aria-expanded="true" data-toggle="collapse" class="pull-right"><i class="fa fa-angle-down"></i></a></h5>
                                <div class="panel-collapse collapse show" id="TerminateAccount">
                                    <ul class="acc-list">
                                        <li><a href="my-payment"><i class="icon-cancel-circled "></i> ชำระเงินค่าลงงาน </a></li>
                                    </ul>
                                </div>
                            </div> -->
                            
                            <!-- /.collapse-box  -->
                        </div>
                    </div>
                    <!-- /.inner-box  -->

                </aside>
            </div>
            <!--/.page-sidebar-->

            <div class="col-md-9 page-content">
               

                <div class="inner-box">
                    <div class="welcome-msg">
                        <h3 class="page-sub-header2 clearfix no-padding"><?php echo $profile['email']; ?> </h3>
                        <span class="page-sub-header-sub small"><?php echo (empty($profile['update_at']) ? 'ยังไม่มีข้อมูลอัพเดท' : 'คุณอัพเดทข้อมูลล่าสุดเมื่อ' . $profile['update_at']); ?></span>
                    </div>
                    <div id="accordion" class="panel-group">
                        <div class="card card-default">
                            <div class="card-header">
                                <h4 class="card-title"><a href="#collapseB1" aria-expanded="true" data-toggle="collapse"> My
                                        details </a></h4>
                            </div>
                            <div class="panel-collapse collapse show" id="collapseB1">
                                <div class="card-body">
                                    <form action="My-profile-update" class="form-horizontal" role="form" method="POST">
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">First Name</label>
                                            <div class="col-sm-9">
                                                <input type="hidden" name="id" value="<?php echo $profile['id'] ?>">
                                                <input type="text" class="form-control" name="first_name" value="<?php echo $profile['first_name']; ?>">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">Last name</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" name="last_name" value="<?php echo $profile['last_name']; ?>">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">Gender</label>
                                            <div class="col-sm-9">
                                                <div class="col-md-6">
                                                    <div class="radio">
                                                        <label for="Gender-0">
                                                            <input name="gender" id="Gender-0" value="ชาย" checked="checked" type="radio" <?php echo ($profile['gender'] == 'ชาย' ? 'checked' : ''); ?>>ชาย </label>
                                                    </div>
                                                    <div class="radio">
                                                        <label for="Gender-1">
                                                            <input name="gender" id="Gender-1" value="หญิง" type="radio" <?php echo ($profile['gender'] == 'หญิง' ? 'checked' : ''); ?>>หญิง </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="Phone" class="col-sm-3 control-label">Phone</label>

                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" id="Phone" name="tel" value="<?php echo $profile['tel']; ?>">
                                            </div>
                                        </div>

                                        <div class="form-group hide">
                                            <!-- remove it if dont need this part -->
                                            <label class="col-sm-3 control-label">Facebook account map</label>

                                            <div class="col-sm-9">
                                                <div class="form-control"><a class="link" href="fb.com">Jhone.doe</a> <a class=""> <i class="fa fa-minus-circle"></i></a></div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-offset-3 col-sm-9"></div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-offset-3 col-sm-9">
                                                <button type="submit" class="btn btn-default">Update</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--/.row-box End-->

                </div>
            </div>
            <!--/.page-content-->
        </div>
        <!--/.row-->
    </div>
    <!--/.container-->
</div>
<!-- /.main-container -->