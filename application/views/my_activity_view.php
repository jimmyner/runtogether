<?php $id            = $this->input->get('id'); ?>
<?php $profile       = $this->db->get_where('tbl_member', ['email' => $this->session->userdata('email')])->row_array(); ?>
<?php $event         = $this->db->get_where('tbl_event', ['id' => $id, 'member_id' =>  $profile['id']])->row_array(); ?>
<?php $type          = $this->db->get('tbl_type')->result_array(); ?>
<div class="main-container">
    <div class="container">
        <div class="row">
            <div class="col-md-3 page-sidebar">
                <aside>
                    <div class="inner-box">
                        <div class="user-panel-sidebar">
                            <div class="collapse-box">
                                <h5 class="collapse-title no-border"> โปรไฟล์ของฉัน <a href="#MyClassified" aria-expanded="true" data-toggle="collapse" class="pull-right"><i class="fa fa-angle-down"></i></a></h5>

                                <div class="panel-collapse collapse show" id="MyClassified">
                                    <ul class="acc-list">
                                        <li><a href="My-profile"><i class="icon-home"></i>
                                                โปรไฟล์ของฉัน </a></li>

                                    </ul>
                                </div>
                            </div>
                            <!-- /.collapse-box  -->
                            <div class="collapse-box">
                                <h5 class="collapse-title"> เครื่องมือต่างๆ <a href="#MyAds" aria-expanded="true" data-toggle="collapse" class="pull-right"><i class="fa fa-angle-down"></i></a></h5>

                                <div class="panel-collapse collapse show" id="MyAds">
                                    <ul class="acc-list">
                                        <li><a href="my-activity"><i class="icon-docs"></i> ลงทะเบียนงานวิ่ง </a></li>
                                        <li><a class="active" href="my-order"><i class="icon-heart"></i> ประวัติงานวิ่ง </a></li>
                                    </ul>
                                </div>
                            </div>

                        </div>
                    </div>
                    <!-- /.inner-box  -->

                </aside>
            </div>
            <!--/.page-sidebar-->

            <div class="col-md-9 page-content">

                <div class="inner-box">
                    <div class="welcome-msg">
                        <h3 class="page-sub-header2 clearfix no-padding"><?php echo $profile['email']; ?> </h3>
                        <span class="page-sub-header-sub small"><?php echo (empty($profile['update_at']) ? 'ยังไม่มีข้อมูลอัพเดท' : 'คุณอัพเดทข้อมูลล่าสุดเมื่อ' . $profile['update_at']); ?></span>
                    </div>
                    <div id="accordion" class="panel-group">
                        <div class="card card-default">
                            <div class="card-header">
                                <h4 class="card-title"><a href="#collapseB1" aria-expanded="true" data-toggle="collapse"> รายละเอียดงานวิ่ง </a></h4>
                            </div>
                            <div class="panel-collapse collapse show" id="collapseB1">
                                <div class="card-body">

                                    <input type="hidden" name="member_id" value="<?php echo $profile['id'];   ?>">
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">ชื่องานวิ่ง</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" name="event_name" value="<?php echo $event['name_event']; ?>" readonly>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">รายละเอียดกิจกรรม</label>
                                        <div class="col-sm-9">
                                            <textarea class="form-control" id="froala-editor" name="detail" placeholder="" rows="7" readonly><?php echo $event['details_event']; ?></textarea>
                                        </div>
                                    </div>
                                    <div class="form-group" style="margin-left: 13px;">
                                        <div class="row">
                                            <div class="col-sm-3">
                                                <label class="control-label">ไซต์ S</label>
                                                <input type="text" class="form-control" name="S" value="<?php echo $event['size_s']; ?>" readonly>
                                            </div>
                                            <div class="col-sm-3">
                                                <label class="control-label">ไซต์ M</label>
                                                <input type="text" class="form-control" name="M" value="<?php echo $event['size_m']; ?>" readonly>
                                            </div>
                                            <div class="col-sm-3">
                                                <label class="control-label">ไซต์ L</label>
                                                <input type="text" class="form-control" name="L" value="<?php echo $event['size_l']; ?>" readonly>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group" style="margin-left: 13px;">
                                        <div class="row">
                                            <div class="col-sm-3">
                                                <label class="control-label">ไซต์ XL</label>
                                                <input type="text" class="form-control" name="XL" value="<?php echo $event['size_xl']; ?>" readonly>
                                            </div>
                                            <div class="col-sm-3">
                                                <label class="control-label">ไซต์ oversize</label>
                                                <input type="text" class="form-control" name="over" value="<?php echo $event['size_oversize']; ?>" readonly>
                                            </div>
                                        </div>
                                    </div>

                                    <!-- Multiple Radios -->
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">แพ๊คเกจงานวิ่ง</label>
                                        <div class="col-md-9">
                                            <?php foreach ($type as $key => $type) : ?>
                                                <div class="radio">
                                                    <label for="Gender-0" style="font-size: 16px;">
                                                        <input name="package" value="<?php echo $type['id']; ?>" type="radio" <?php echo $type['id'] == $event['type_event'] ? 'checked' : '';  ?>>
                                                        <?php echo $type['name'] . ' ' . $type['people'] . ' คน (' . $type['price'] . ' บาท)'; ?> </label>
                                                </div>
                                            <?php endforeach; ?>
                                        </div>
                                    </div>

                                    <div class="form-group" style="margin-left: 13px;">
                                        <div class="row">
                                            <div class="col-sm-4">
                                                <label class="control-label">ภาพหน้าปก</label>
                                                <img src="uploads/event/<?php echo $event['file_cover']; ?>" alt="" style="width: 100%">
                                            </div>
                                            <div class="col-sm-4">
                                                <label class="control-label">ภาพเสื้องานวิ่ง</label>
                                                <img src="uploads/event/<?php echo $event['file_shirt']; ?>" alt="" style="width: 100%">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">เวลาปิดรับสมัคร</label>
                                        <div class="col-sm-4">
                                            <input type="date" class="form-control" name="date" value="<?php echo $event['time_out']; ?>" readonly>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">เลขบัญชี</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" name="account_number" value="<?php echo $event['account_number']; ?>" readonly>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-sm-offset-3 col-sm-9"></div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                    <!--/.row-box End-->

                </div>
            </div>
            <!--/.page-content-->
        </div>
        <!--/.row-->
    </div>
    <!--/.container-->
</div>
<!-- /.main-container -->