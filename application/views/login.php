
    <div class="main-container">
        <div class="container">
            <div class="row">
                <div class="col-sm-5 login-box">
                    <div class="card card-default">
                        <div class="panel-intro text-center">
                            <h2 class="logo-title">
                                <!-- Original Logo will be placed here  -->
                                <span class="logo-icon"><i
                                        class="icon icon-search-1 ln-shadow-logo shape-0"></i> </span> RUN<span>TOGETHER </span>
                            </h2>
                        </div>
                        <div class="card-body">
                            <form role="form" class="loginForm" method="POST" action="LoginMe">
                                <div class="form-group">
                                    <label for="sender-email" class="control-label">อีเมล์:</label>

                                    <div class="input-icon"><i class="icon-user fa"></i>
                                        <input id="sender-email" type="email" placeholder="อีเมล์"
                                               class="form-control email" name="email" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="user-pass" class="control-label">รหัสผ่าน:</label>

                                    <div class="input-icon"><i class="icon-lock fa"></i>
                                        <input type="password" class="form-control" placeholder="รหัสผ่าน"
                                               id="user-pass" name="password" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <input name="submit" class="btn btn-primary btn-block" value="เข้าสู่ระบบ"
                                           type="submit">
                                </div>
                            </form>
                        </div>
                        <div class="card-footer">

                            <div class="checkbox pull-left">
                                <label class="custom-control custom-checkbox mb-2 mr-sm-2 mb-sm-0">
                                    <input type="checkbox" class="custom-control-input">
                                    <span class="custom-control-indicator"></span>
                                    <!-- <span class="custom-control-description"> Keep me logged in</span> -->
                                </label>
                            </div>

                            <div style=" clear:both"></div>
                        </div>
                    </div>
                    <div class="login-box-btm text-center">
                        <p> คุณต้องการจะสมัครเป็นพ่อค้าหรือ ? <br>
                            <a href="Register"><strong>สมัครสมาชิก !</strong> </a></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /.main-container -->
