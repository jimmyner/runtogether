<?php $event  = $this->db->get_where('tbl_event',['status' => '2'])->result_array();  ?>
<?php $event2 = $this->db->get_where('tbl_event',['status' => '2'])->result_array();  ?>

<div class="search-row-wrapper events" style="background-image: url(images/events/5-large_x2.jpg)">
    <div class="inner">
        <div class="container ">
            <form action="allrun_search" method="GET">
                <div class="row">

                    <div class="col-md-9">
                        <input class="form-control keyword" type="text" name="keyword" placeholder="Events title">
                    </div>

                    <div class="col-md-3">
                        <button class="btn btn-block btn-primary  "> <i class="fa fa-search"></i> Find Events
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- /.search-row -->
<div class="main-container">
    <div class="container">
        <div class="row">
            <?php if ($event) : ?>
            <!-- this (.mobile-filter-sidebar) part will be position fixed in mobile version -->
            <div class="col-md-3 page-sidebar mobile-filter-sidebar sidebar-modern">
                <aside>
                    <div class="sidebar-modern-inner">
                        <div class="block-title sidebar-header">
                            <h5><a href="#">รายชื่องานวิ่ง</a></h5>
                        </div>
                        <div class="block-content categories-list  list-filter ">

                            <ul class=" list-unstyled">
                                <?php foreach ($event2 as $key => $event2) : ?>
                                <li><a href="event-details?id=<?php echo $event2['id']; ?>"><span class="title"><?php echo $event2['name_event']; ?></span><span class="count">&nbsp;</span></a></li>
                                <?php endforeach; ?>
                            </ul>
                        </div>
                        <!--/.categories-list-->

                        <div style="clear:both"></div>
                    </div>

                    <!--/.categories-list-->
                </aside>
            </div>
            <!--/.page-side-bar-->
            <div class="col-md-9 page-content col-thin-left">
                <div class="category-list event-category-list">

                 
                    <!-- Mobile Filter bar-->
                    <div class="mobile-filter-bar col-xl-12  ">
                        <ul class="list-unstyled list-inline no-margin no-padding">
                            <li class="filter-toggle">
                                <a class="">
                                    <i class="  icon-th-list"></i>
                                    Filters
                                </a>
                            </li>
                            <li>


                                <div class="dropdown"> <a data-toggle="dropdown" class="dropdown-toggle"> Short

                                        by </a>
                                    <ul class="dropdown-menu">
                                        <li class="dropdown-item"><a href="" rel="nofollow">Relevance</a>
                                        </li>
                                        <li class="dropdown-item"><a href="" rel="nofollow">Date</a>
                                        </li>
                                        <li class="dropdown-item"><a href="" rel="nofollow">Company</a>
                                        </li>
                                    </ul>
                                </div>

                            </li>
                        </ul>
                    </div>
                    <div class="menu-overly-mask"></div>
                    <!-- Mobile Filter bar End-->

                    <div class="adds-wrapper">
                        <div class="row" style="width: 100%;">
                        <?php foreach ($results as $key => $event) : ?>
                        <?php $member = $this->db->get_where('tbl_member',['id' => $event['member_id']])->row_array();  ?>
                            <div class="col-md-4 col-sm-6 col-12 event-item-col">
                                <div class="card card-event info-overlay">
                                    <div class="img has-background" style="background-image: url(uploads/event/<?php echo $event['file_cover']; ?>); background-size:cover ">
                                        <div class="pop-info ">
                                            <span class="event-badges ">
                                                <span class="badge price-tag big badge-default"> $292</span>
                                            </span>
                                        </div>

                                        <!-- //
                                            To make 100% fit to box & responsive i used a fake image based on actual image ratio.
                                            Tf you think you will use all same size image then you can remove background image and
                                            use image below as a main src image.
                                            -->
                                        <a href="event-details?id=<?php echo $event['id']; ?>">
                                            <img alt="340x230" class="card-img-top img-responsive" data-holder-rendered="true" src="images/10x6.gif"> </a>
                                    </div>
                                    <div class="card-body">
                                        <h4 class="card-title">
                                            <a href="event-details?id=<?php echo $event['id']; ?>"><?php echo $event['name_event']; ?></a>
                                        </h4>
                                        <p class="card-text hide">Donec imperdiet leo ac ipsum blandit auctor.</p>

                                        <div class="card-event-info">
                                            <p class="event-location"><i class="fa icon-location-1"></i>
                                                <a class="location" href="">เวลาปิดรับสมัคร <?php echo $event['time_out']; ?></a></p>
                                            <p class="event-time"><i class="fa icon-calendar-1"></i> วันที่สร้างโพส <?php echo $event['create_at']; ?>  </p>
                                        </div>
                                    </div>
                                    <div class="card-footer">
                                        <div class="pull-left left">
                                            <div class="">by <a href="aurthor.html"><?php  echo $member['first_name'].' '.$member['last_name']; ?> </a></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php endforeach; ?>

                        </div>
                    </div>
                    <!--/.adds-wrapper-->

                    <div class="tab-box  save-search-bar text-center"></div>
                </div>

                <!--/.pagination-bar -->

                <!--/.post-promo-->

            </div>
            <!--/.page-content-->

            <?php else : ?>

                <div class="col-sm-12 text-center">
                    <h1 style="margin: 100px 0;">ยังไม่มีข้อมูล ข่าวสาร</h1>
                </div>

            <?php endif; ?>

        </div>
    </div>
</div>
<!-- /.main-container -->