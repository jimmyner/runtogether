<?php $get_id = $this->input->get('id'); ?>
<?php $get_event = $this->db->get_where('tbl_event', ['id' => $get_id])->row_array(); ?>
<?php $session = $this->session->userdata('email'); ?>
<?php $marathon = $this->db->get_where('tbl_marathon', ['id_event' => $get_id])->result_array();?>
<?php $ageAll = $this->db->get_where('tbl_age', ['id_event' => $get_id])->result_array();?>
<?php $data = date('Y-m-d') ?>
<?php
$month = array(
    '01'  => 'มกราคม', '02'  => 'กุมภาพันธ์', '03'  => 'มีนาคม',
    '04'  => 'เมษายน', '05'  => 'พฤษภาคม', '06'  => 'มิถุนายน',
    '07'  => 'กรกฎาคม', '08'  => 'สิงหาคม', '09'  => 'กันยายน',
    '10'  => 'ตุลาคม', '11'  => 'พฤศจิกายน', '12'  => 'ธันวาคม',
);
function thaiDate($datetime)
{
    list($date) = explode(' ', $datetime); // แยกวันที่ กับ เวลาออกจากกัน
    list($Y, $m, $d) = explode('-', $date); // แยกวันเป็น ปี เดือน วัน
    $Y = $Y; // เปลี่ยน ค.ศ. เป็น พ.ศ.
    switch ($m) {
        case "01":
            $m = "มกราคม";
            break;
        case "02":
            $m = "กุมภาพันธ์";
            break;
        case "03":
            $m = "มีนาคม";
            break;
        case "04":
            $m = "เมษายน";
            break;
        case "05":
            $m = "พฤษภาคม";
            break;
        case "06":
            $m = "มิถุนายน";
            break;
        case "07":
            $m = "กรกฎาคม";
            break;
        case "08":
            $m = "สิงหาคม";
            break;
        case "09":
            $m = "กันยายน";
            break;
        case "10":
            $m = "ตุลาคม";
            break;
        case "11":
            $m = "พฤศจิกายน ";
            break;
        case "12":
            $m = "ธันวาคม";
            break;
    }
    return $d . " " . $m . " " . $Y;
}
?>
<div class="main-container">
    <div class="container">
        <div class="row">
            <div class="col-md-12">

                <nav aria-label="breadcrumb" role="navigation" class="pull-left">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="Index"><i class="icon-home fa"></i></a></li>
                        <li class="breadcrumb-item"><a href="#">รายละเอียดงานวิ่ง</a></li>
                        <li class="breadcrumb-item active" aria-current="page"><?php echo $get_event['name_event']; ?></li>
                    </ol>
                </nav>
            </div>
        </div>

    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-8 page-content col-thin-right">
                <div class="ev-image">
                    <img class="img-responsive" src="uploads/event/<?php echo $get_event['file_cover']; ?>" alt="img">
                </div>
                <div class="event-details-title">
                    <h1 class="title" style="font-size:28px;">
                        <span class="auto-title left"><?php echo $get_event['name_event']; ?></span>
                    </h1>

                </div>
                <div class="inner inner-box ads-details-wrapper event">


                    <div class="Ads-Details">
                        <h4 class="text-uppercase"><strong>เกี่ยวกับงานวิ่ง </strong></h4>

                        <div class="ads-details-info">
                            <p><?php echo $get_event['details_event']; ?></p><br>
                            <p><b>เปิดรับสมัคร : <span style="color:green">วันนี้ - <?php echo thaiDate($get_event['time_out']); ?></span></b></p>
                            <br>
                            <h4 class="text-uppercase"><strong>แบบเสื้อวิ่ง</strong></h4>
                            <img class="img-responsive" src="uploads/event/<?php echo $get_event['file_shirt']; ?>" alt="img">
                        </div>
                    </div>
                </div>
                <!--/.ads-details-wrapper-->

            </div>
            <!--/.page-content-->

            <div class="col-md-4  page-sidebar-right">
                <aside>
                    <div class="card sidebar-card">
                        <div class="card-header">EVENT DETAILS</div>
                        <div class="card-content">
                            <div class="ev-action">
                                <div class="form-group text-center">
                                    ปิดรับสมัครวันที่ : <?php echo $get_event['time_out']; ?>
                                </div>
                                <div class="form-group text-center">
                                    เลขที่บัญชีธนาคาร : <?php echo $get_event['account_number']; ?>
                                </div>
                                <div class="form-group text-center">
                                    ธนาคาร : <?php echo $get_event['bank']; ?>
                                </div>
                                <?php if ($session) { ?>
                                    <a class="btn btn-primary btn-block" onclick="myFunction()"> สมัครงานวิ่ง</a>
                                <?php } else { ?>
                                    <?php if ($data > $get_event['time_out'] ) : ?>
                                        <a class="btn btn-light btn-block"> หมดเวลาสมัครงานวิ่ง</a>

                                    <?php else: ?>
                                        <a class="btn btn-primary btn-block" href="#exampleModal2" data-toggle="modal"> สมัครงานวิ่ง</a>

                                    <?php endif; ?>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                    <!--/.categories-list-->
                </aside>
            </div>
            <!--/.page-side-bar-->
        </div>
    </div>
</div>
<!-- /.main-container -->
<!-- Modal -->
<div class="modal fade" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel"> กรอกข้อมูลส่วนตัว</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="send-event" method="post" enctype="multipart/form-data">
                <div class="modal-body">
                    <input type="hidden" name="get_id" value="<?php echo $get_id; ?>">

                    <label style="color:red">* <span style="color:#000;">ชื่อ-นามสกุล</span></label>
                    <input type="text" class="form-control" value="" name="full_name" required>

                    <label style="color:red">* <span style="color:#000;">อายุ</span></label>
                    <input type="text" class="form-control" value="" name="age" maxlength="2" OnKeyPress="return chkNumber(this)" required>

                    <label style="color:red">* <span style="color:#000;">เลขบัตรประชาชน</span></label>
                    <input type="text" class="form-control" value="" name="card" minlength="13" maxlength="20" OnKeyPress="return chkNumber(this)" required>

                    <label style="color:red">* <span style="color:#000;">รายการวิ่ง</span></label>
                    <select class="form-control" name="marathon" id="" required>
                        <?php foreach ($marathon as $marathonDetail) { ?>
                        <option value="<?php echo $marathonDetail['id']; ?>"><?php echo $marathonDetail['name_marathon']." ระยะทาง ".$marathonDetail['length']." กิโลเมตร ราคา ".$marathonDetail['price']." บาท"; ?></option>
                        <?php } ?>
                    </select>

                    <label style="color:red">* <span style="color:#000;">ช่วงรุ่นอายุ</span></label>
                    <select class="form-control" name="age_rank" id="" required>
                        <?php foreach ($ageAll as $age) { ?>
                        <option value="<?php echo $age['id']; ?>"><?php echo $age['age_title']; ?></option>
                        <?php } ?>
                    </select>

                    <label style="color:red">* <span style="color:#000;">Size เสื้อ</span></label>
                    <select class="form-control" name="size" id="" required>
                        <option value="<?php echo "S ".$get_event['size_s']; ?>"><?php echo "S รอบอก ".$get_event['size_s']; ?></option>
                        <option value="<?php echo "M ".$get_event['size_m']; ?>"><?php echo "M รอบอก ".$get_event['size_m']; ?></option>
                        <option value="<?php echo "L ".$get_event['size_l']; ?>"><?php echo "L รอบอก ".$get_event['size_l']; ?></option>
                        <option value="<?php echo "XL ".$get_event['size_xl']; ?>"><?php echo "XL รอบอก ".$get_event['size_xl']; ?></option>
                        <option value="<?php echo "OverSize ".$get_event['size_oversize']; ?>"><?php echo "OverSize รอบอก ".$get_event['size_oversize']; ?></option>
                    </select>


                    <label style="color:red">* <span style="color:#000;">เบอร์โทรติดต่อ</span></label>
                    <input type="text" class="form-control" value="" minlength="9" maxlength="10" name="tel" OnKeyPress="return chkNumber(this)" required>

                    <label style="color:red">* <span style="color:#000;">แนบหลักฐานการจ่ายเงิน</span></label>
                    <input type="file" class="form-control" value="" name="file_name" required>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>

        </div>
    </div>
</div>
<script language="JavaScript">
    function chkNumber(ele) {
        var vchar = String.fromCharCode(event.keyCode);
        if ((vchar < '0' || vchar > '9') && (vchar != '.')) return false;
        ele.onKeyPress = vchar;
    }
</script>
<script>
    function myFunction() {
        alert("ผู้ประกาศงานวิ่งจะไม่สามารถสมัครงานวิ่งอื่นๆได้ กรุณาออกจากระบบค่ะ !!!");
    }
</script>