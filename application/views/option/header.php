<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Fav and touch icons -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="assets/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="assets/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="assets/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="assets/ico/apple-touch-icon-57-precomposed.png">
    <link rel="shortcut icon" href="assets/ico/favicon.png">
    <title>RUN TOGETHER</title>
    <!-- Bootstrap core CSS -->
    <link href="assets/bootstrap/css/bootstrap.css" rel="stylesheet">


    <link href="assets/css/style.css" rel="stylesheet">

    <!-- styles needed for carousel slider -->
    <link href="assets/plugins/owl-carousel/owl.carousel.css" rel="stylesheet">
    <link href="assets/plugins/owl-carousel/owl.theme.css" rel="stylesheet">

	<!-- bxSlider CSS file -->
	<link href="assets/plugins/bxslider/jquery.bxslider.css" rel="stylesheet" />

	<!-- Include Editor style. -->
	<link href='https://cdn.jsdelivr.net/npm/froala-editor@3.1.0/css/froala_editor.pkgd.min.css' rel='stylesheet' type='text/css' />

	<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.3/moment.min.js"></script>
	

    <!-- Just for debugging purposes. -->
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

    <!-- include pace script for automatic web page progress bar  -->
    <script>
        paceOptions = {
            elements: true
        };
    </script>
    <script src="assets/js/pace.min.js"></script>
	<script src="assets/plugins/modernizr/modernizr-custom.js"></script>
	


</head>
<body>
<div id="wrapper">


    <div class="header">
    	<nav class="navbar navbar-site navbar-light navbar-bodered bg-light navbar-expand-md"
    		 role="navigation">
    		<div class="container">

    			<div class="navbar-identity">


    				<a href="Index" class="navbar-brand logo logo-title">
    			<span class="logo-icon"><i class="icon  icon-calendar-1 ln-shadow-logo "></i>
    			</span>RUN<span>TOGETHER </span> </a>

    				<button data-target=".navbar-collapse" data-toggle="collapse" class="navbar-toggler pull-right"
    						type="button">

    					<svg xmlns="http://www.w3.org/2000/svg" viewbox="0 0 30 30" width="30" height="30" focusable="false"><title>Menu</title><path stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-miterlimit="10" d="M4 7h22M4 15h22M4 23h22"/></svg>


    				</button>

    				<button
    						class="flag-menu country-flag d-block d-md-none btn btn-secondary hidden pull-right"
    						href="#select-country" data-toggle="modal">	<span class="flag-icon flag-icon-us"></span>  <span class="caret"></span>
    				</button>

    			</div>

    			<div class="navbar-collapse collapse">
    				<ul class="nav navbar-nav navbar-left">
    					<li class="nav-item"><a class="nav-link" href="Index">หน้าหลัก</a></li>
    					<li class="nav-item"><a class="nav-link" href="Allrun">งานวิ่งทั้งหมด</a></li>
    					<li class="nav-item"><a class="nav-link" href="Blog-article">บทความ</a></li>
    					<li class="nav-item"><a class="nav-link" href="Blog-news">ข่าวสาร</a></li>
    					<li class="nav-item"><a class="nav-link" href="About-us">เกี่ยวกับเรา</a></li>
    					<li class="nav-item"><a class="nav-link" href="Contact">ติดต่อเรา</a></li>
    				</ul>
    				<ul class="nav navbar-nav ml-auto navbar-right">
						<?php if ($this->session->userdata('email') == '') :?>
						<li class="nav-item"><a class="nav-link" href="Login">เข้าสู่ระบบ (เจ้าของงานวิ่ง)</a></li>
						<li class="nav-item"><a class="nav-link" href="#exampleModal" data-toggle="modal"><i class="fa fa-search"></i> ค้นหาประวัติ</a></li>
						<?php else: ?>
						<?php $user = $this->db->get_where('tbl_member',['email' => $this->session->userdata('email')])->row_array(); ?>
    					<li class="dropdown no-arrow nav-item"><a href="#" class="dropdown-toggle nav-link" data-toggle="dropdown">
							
    						<span><?php echo $user['first_name']; ?> <?php echo $user['last_name']; ?></span> <i class="icon-user fa"></i> <i class=" icon-down-open-big fa"></i></a>
    						<ul class="dropdown-menu user-menu dropdown-menu-right">
    							<li class="active dropdown-item"><a href="My-profile"><i class="icon-home"></i> ประวัติส่วนตัว </a></li>
    							<li class="dropdown-item"><a href="my-activity"><i class="icon-th-thumb"></i> ลงกิจกรรมงานวิ่ง </a></li>
    							<li class="dropdown-item"><a href="Logout"><i class=" icon-logout "></i> Log out </a></li>
    						</ul>
						</li>
						<?php endif; ?>

    				</ul>
    			</div>
    			<!--/.nav-collapse -->
    		</div>
    		<!-- /.container-fluid -->
    	</nav>
    </div>
	<!-- /.header -->
	<!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
		  </div>
		  <form action="passpost" method="get">
          <div class="modal-body">
            <label for="" style="color:red">* ค้นให้ประวัติโดยใส่หมายเลขบัตรประชาชน</label>
            <input type="text" class="form-control" value="" name="passpost" placeholder="Search">
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Search</button>
          </div>
		  </form>
        </div>
      </div>
    </div>