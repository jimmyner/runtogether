<?php $profile = $this->db->get_where('tbl_member', ['email' => $this->session->userdata('email')])->row_array(); ?>
<?php $type = $this->db->get('tbl_type')->result_array(); ?>
<div class="main-container">
    <div class="container">
        <div class="row">
            <div class="col-md-3 page-sidebar">
                <aside>
                    <div class="inner-box">
                        <div class="user-panel-sidebar">
                            <div class="collapse-box">
                                <h5 class="collapse-title no-border"> โปรไฟล์ของฉัน <a href="#MyClassified" aria-expanded="true" data-toggle="collapse" class="pull-right"><i class="fa fa-angle-down"></i></a></h5>

                                <div class="panel-collapse collapse show" id="MyClassified">
                                    <ul class="acc-list">
                                        <li><a href="My-profile"><i class="icon-home"></i>
                                                โปรไฟล์ของฉัน </a></li>

                                    </ul>
                                </div>
                            </div>
                            <!-- /.collapse-box  -->
                            <div class="collapse-box">
                                <h5 class="collapse-title"> เครื่องมือต่างๆ <a href="#MyAds" aria-expanded="true" data-toggle="collapse" class="pull-right"><i class="fa fa-angle-down"></i></a></h5>

                                <div class="panel-collapse collapse show" id="MyAds">
                                    <ul class="acc-list">
                                        <li><a class="active" href="my-activity"><i class="icon-docs"></i> ลงทะเบียนงานวิ่ง </a></li>
                                        <li><a href="my-order"><i class="icon-heart"></i> ประวัติงานวิ่ง </a></li>
                                    </ul>
                                </div>
                            </div>
                            <!-- /.collapse-box  -->

                            <!-- <div class="collapse-box">
                                <h5 class="collapse-title"> จุดชำระเงิน <a href="#TerminateAccount" aria-expanded="true" data-toggle="collapse" class="pull-right"><i class="fa fa-angle-down"></i></a></h5>
                                <div class="panel-collapse collapse show" id="TerminateAccount">
                                    <ul class="acc-list">
                                        <li><a href="my-payment"><i class="icon-cancel-circled "></i> ชำระเงินค่าลงงาน </a></li>
                                    </ul>
                                </div>
                            </div> -->

                            <!-- /.collapse-box  -->
                        </div>
                    </div>
                    <!-- /.inner-box  -->

                </aside>
            </div>
            <!--/.page-sidebar-->

            <div class="col-md-9 page-content">


                <div class="inner-box">
                    <div class="welcome-msg">
                        <h3 class="page-sub-header2 clearfix no-padding"><?php echo $profile['email']; ?> </h3>
                        <span class="page-sub-header-sub small"><?php echo (empty($profile['update_at']) ? 'ยังไม่มีข้อมูลอัพเดท' : 'คุณอัพเดทข้อมูลล่าสุดเมื่อ' . $profile['update_at']); ?></span>
                    </div>
                    <div id="accordion" class="panel-group">
                        <div class="card card-default">
                            <div class="card-header">
                                <h4 class="card-title"><a href="#collapseB1" aria-expanded="true" data-toggle="collapse"> My
                                        details </a></h4>
                            </div>
                            <div class="panel-collapse collapse show" id="collapseB1">
                                <div class="card-body">
                                    <form action="my-activity-create" class="form-horizontal" role="form" method="POST" enctype="multipart/form-data">
                                        <input type="hidden" name="member_id" value="<?php echo $profile['id'];   ?>">
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">ชื่องานวิ่ง</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" name="event_name" value="" required>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">รายละเอียดกิจกรรม</label>
                                            <div class="col-sm-9">
                                                <textarea class="form-control" id="froala-editor" name="detail" placeholder=""></textarea>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-12 control-label" style="color:red">กรุณากรอกข้อมูลให้ถูต้อง *</label>
                                            <label class="col-sm-4 control-label">รายละเอียดกิจกรรม</label>
                                            <label class="col-sm-2 control-label">ระยะทาง</label>
                                            <label class="col-sm-3 control-label">ราคา</label>

                                            <div class="row" style="margin: 10px 10px;">
                                                <div class="col-sm-1">
                                                    <input type="checkbox" class="form-control" style="width: 19px;" name="fun" value="fun marathons">
                                                </div>
                                                <div class="col-sm-3" style="display: flex;align-items: center;">
                                                    fun marathons
                                                </div>
                                                <div class="col-sm-2" style="display: flex;align-items: center;">
                                                    <input type="text" class="form-control" name="km_fun" id="">
                                                </div>
                                                <div class="col-sm-2" style="display: flex;align-items: center;">
                                                    <input type="number" class="form-control" name="price_fun" id="">
                                                </div>
                                            </div>

                                            <div class="row" style="margin: 10px 10px;">
                                                <div class="col-sm-1">
                                                    <input type="checkbox" class="form-control" style="width: 19px;" name="mini" value="mini marathons">
                                                </div>
                                                <div class="col-sm-3" style="display: flex;align-items: center;">
                                                    mini marathons
                                                </div>
                                                <div class="col-sm-2" style="display: flex;align-items: center;">
                                                    <input type="text" class="form-control" name="km_mini" id="">
                                                </div>
                                                <div class="col-sm-2" style="display: flex;align-items: center;">
                                                    <input type="number" class="form-control" name="price_mini" id="">
                                                </div>
                                            </div>

                                            <div class="row" style="margin: 10px 10px;">
                                                <div class="col-sm-1">
                                                    <input type="checkbox" class="form-control" style="width: 19px;" name="half" value="half marathons">
                                                </div>
                                                <div class="col-sm-3" style="display: flex;align-items: center;">
                                                    half marathons
                                                </div>
                                                <div class="col-sm-2" style="display: flex;align-items: center;">
                                                    <input type="text" class="form-control" name="km_half" id="">
                                                </div>
                                                <div class="col-sm-2" style="display: flex;align-items: center;">
                                                    <input type="text" class="form-control" name="price_half" id="">
                                                </div>
                                            </div>

                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-4 control-label">รุ่นอายุ </label>

                                            <div class="row" style="margin: 10px 10px;">
                                                <div class="col-sm-1">
                                                    <input type="checkbox" class="form-control" style="width: 19px;" name="age[]" value="รุ่นอายุต่ำกว่า 19 ปี">
                                                </div>
                                                <div class="col-sm-3" style="display: flex;align-items: center;">
                                                    รุ่นอายุต่ำกว่า 19 ปี
                                                </div>
                                            </div>

                                            <div class="row" style="margin: 10px 10px;">
                                                <div class="col-sm-1">
                                                    <input type="checkbox" class="form-control" style="width: 19px;" name="age[]" value="รุ่นอายุต่ำกว่า 20 - 30 ปี">
                                                </div>
                                                <div class="col-sm-3" style="display: flex;align-items: center;">
                                                    รุ่นอายุต่ำกว่า 20 - 30 ปี
                                                </div>

                                            </div>

                                            <div class="row" style="margin: 10px 10px;">
                                                <div class="col-sm-1">
                                                    <input type="checkbox" class="form-control" style="width: 19px;" name="age[]" value="รุ่นอายุมากกว่า 31 - 40 ปี">
                                                </div>
                                                <div class="col-sm-3" style="display: flex;align-items: center;">
                                                    รุ่นอายุมากกว่า 31 - 40 ปี
                                                </div>

                                            </div>

                                            <div class="row" style="margin: 10px 10px;">
                                                <div class="col-sm-1">
                                                    <input type="checkbox" class="form-control" style="width: 19px;" name="age[]" value="4,รุ่นอายุมากกว่า 41 ปี ขึ้นไป">
                                                </div>
                                                <div class="col-sm-3" style="display: flex;align-items: center;">
                                                    รุ่นอายุมากกว่า 41 ปี ขึ้นไป
                                                </div>

                                            </div>

                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-6 control-label">รอบอกเสื้อวิ่ง</label>
                                        </div>

                                        <div class="form-group" style="margin-left: 13px;">
                                            <div class="row">
                                                <div class="col-sm-3">
                                                    <label class="control-label">ไซต์ S</label>
                                                    <input type="text" class="form-control" name="S" value="" required>
                                                </div>
                                                <div class="col-sm-3">
                                                    <label class="control-label">ไซต์ M</label>
                                                    <input type="text" class="form-control" name="M" value="" required>
                                                </div>
                                                <div class="col-sm-3">
                                                    <label class="control-label">ไซต์ L</label>
                                                    <input type="text" class="form-control" name="L" value="" required>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group" style="margin-left: 13px;">
                                            <div class="row">
                                                <div class="col-sm-3">
                                                    <label class="control-label">ไซต์ XL</label>
                                                    <input type="text" class="form-control" name="XL" value="">
                                                </div>
                                                <div class="col-sm-3">
                                                    <label class="control-label">ไซต์ oversize</label>
                                                    <input type="text" class="form-control" name="over" value="">
                                                </div>
                                            </div>
                                        </div>

                                        <!-- Multiple Radios -->
                                        <div class="form-group">
                                            <label class="col-md-4 control-label">แพ๊คเกจงานวิ่ง</label>
                                            <div class="col-md-9">
                                                <?php foreach ($type as $key => $type) : ?>
                                                    <div class="radio">
                                                        <label for="Gender-0" style="font-size: 16px;">
                                                            <input name="package" value="<?php echo $type['id']; ?>" type="radio" required>
                                                            <?php echo $type['name'] . ' ' . $type['people'] . ' คน (' . $type['price'] . ' บาท)'; ?> </label>
                                                    </div>
                                                <?php endforeach; ?>
                                            </div>
                                        </div>

                                        <div class="form-group" style="margin-left: 13px;">
                                            <div class="row">
                                                <div class="col-sm-4">
                                                    <label class="control-label">ภาพหน้าปก</label>
                                                    <input type="file" class="form-control" name="file_nameA" value="" required>
                                                </div>
                                                <div class="col-sm-4">
                                                    <label class="control-label">ภาพเสื้องานวิ่ง</label>
                                                    <input type="file" class="form-control" name="file_nameB" value="" required>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">เวลาปิดรับสมัคร</label>
                                            <div class="col-sm-4">
                                                <input type="date" class="form-control" name="date" value="" min="<?php echo date('Y-m-d'); ?>" required>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">ชื่อธนาคาร</label>
                                            <div class="col-sm-9">
                                                <select class="form-control" name="bank" id="" required>
                                                    <option value="" disabled selected> --- กรุณาเลือกธนาคาร ---</option>
                                                    <option value="ธนาคารกรุงเทพ">ธนาคารกรุงเทพ</option>
                                                    <option value="ธนาคารกรุงไทย">ธนาคารกรุงไทย</option>
                                                    <option value="ธนาคารกรุงศรีอยุธยา">ธนาคารกรุงศรีอยุธยา</option>
                                                    <option value="ธนาคารกสิกรไทย">ธนาคารกสิกรไทย</option>
                                                    <option value="ธนาคารเกียรตินาคิน">ธนาคารเกียรตินาคิน</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">เลขบัญชี</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" name="account_number" value="" required>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-sm-offset-3 col-sm-9"></div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-offset-3 col-sm-9">
                                                <button type="submit" class="btn btn-default">บันทึก</button>
                                            </div>
                                        </div>

                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--/.row-box End-->

                </div>
            </div>
            <!--/.page-content-->
        </div>
        <!--/.row-->
    </div>
    <!--/.container-->
</div>
<!-- /.main-container -->