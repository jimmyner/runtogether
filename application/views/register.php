
    <div class="main-container">
        <div class="container">
            <div class="row">
                <div class="col-md-8 page-content">
                    <div class="inner-box category-content">
                        <h2 class="title-2"><i class="icon-user-add"></i> สร้างบัญชีผู้ใช้งาน ฟรี!! </h2>

                        <div class="row">
                            <div class="col-sm-12">
                                <form class="form-horizontal" action="RegisterMe" method="POST">
                                    <fieldset>
                                        <!-- Text input-->
                                        <div class="form-group  row required">
                                            <label class="col-md-4 control-label">ชื่อ <sup>*</sup></label>

                                            <div class="col-md-6">
                                                <input name="f_name" placeholder="First Name" class="form-control input-md"
                                                       required="" type="text">
                                            </div>
                                        </div>
                                        <!-- Text input-->
                                        <div class="form-group  row required">
                                            <label class="col-md-4 control-label">นามสกุล <sup>*</sup></label>
                                            <div class="col-md-6">
                                                <input name="l_name" placeholder="Last Name"
                                                       class="form-control input-md" type="text" required="">
                                            </div>
                                        </div>

                                        <!-- Text input-->
                                        <div class="form-group  row required">
                                            <label class="col-md-4 control-label">เบอร์โทร <sup>*</sup></label>

                                            <div class="col-md-6">
                                                <input name="tel" placeholder="Phone Number"
                                                       class="form-control input-md" type="text" required="">
                                            </div>
                                        </div>

                                        <!-- Multiple Radios -->
                                        <div class="form-group row">
                                            <label class="col-md-4 control-label">เพศ</label>

                                            <div class="col-md-6">
                                                <div class="radio">
                                                    <label for="Gender-0">
                                                        <input name="Gender" id="Gender-0" value="ชาย" checked="checked"
                                                               type="radio">
                                                        ชาย </label>
                                                </div>
                                                <div class="radio">
                                                    <label for="Gender-1">
                                                        <input name="Gender" id="Gender-1" value="หญิง" type="radio">
                                                        หญิง </label>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group  row required">
                                            <label for="inputEmail3" class="col-md-4 control-label">อีเมล์
                                                <sup>*</sup></label>

                                            <div class="col-md-6">
                                                <input type="email" class="form-control" id="inputEmail3"
                                                       placeholder="Email" name="email" required="">
                                            </div>
                                        </div>

                                        <div class="form-group  row required">
                                            <label for="inputPassword3" class="col-md-4 control-label">รหัสผ่าน </label>

                                            <div class="col-md-6">
                                                <input type="password" class="form-control" id="inputPassword3"
                                                       placeholder="Password" name="password" required="">


                                                    <small id="passwordHelpBlock" class="form-text text-muted">


                                                    At least 5 characters
                                                        </small>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-md-4 control-label"></label>

                                            <div class="col-md-8">
                                                <div class="termbox mb10">
                                                    <div class="col-auto my-1 no-padding">
                                                        <div class="custom-control custom-checkbox mr-sm-2">
                                                            <input type="checkbox" class="custom-control-input" id="customControlAutosizing" required="">
                                                            <label class="custom-control-label" for="customControlAutosizing">  
                                                                <span class="custom-control-description">
                                                                    ฉันได้อ่านและยอมรับ <a href="" data-toggle="modal" data-target="#exampleModalLong">ข้อตกลงและเงื่อนไข</a> 
                                                                </span>
                                                            </label>
                                                        </div>
                                                    </div>

                                                </div>
                                                <div style="clear:both"></div>
                                                <button type="submit" class="btn btn-primary" href="RegisterMe">สมัครสมาชิก</button></div>
                                        </div>
                                    </fieldset>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.page-content -->

                <div class="col-md-4 reg-sidebar">
                    <div class="reg-sidebar-inner text-center">
                        <div class="promo-text-box"><i class=" icon-picture fa fa-4x icon-color-1"></i>

                            <h3><strong>Post a Free Classified</strong></h3>

                            <p> Post your free online classified ads with us. Lorem ipsum dolor sit amet, consectetur
                                adipiscing elit. </p>
                        </div>
                        <div class="promo-text-box"><i class=" icon-pencil-circled fa fa-4x icon-color-2"></i>

                            <h3><strong>Create and Manage Items</strong></h3>

                            <p> Nam sit amet dui vel orci venenatis ullamcorper eget in lacus.
                                Praesent tristique elit pharetra magna efficitur laoreet.</p>
                        </div>
                        <div class="promo-text-box"><i class="  icon-heart-2 fa fa-4x icon-color-3"></i>

                            <h3><strong>Create your Favorite ads list.</strong></h3>

                            <p> PostNullam quis orci ut ipsum mollis malesuada varius eget metus.
                                Nulla aliquet dui sed quam iaculis, ut finibus massa tincidunt.</p>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container -->
    </div>
    <!-- /.main-container -->
<!-- Modal -->
<div class="modal fade" id="exampleModalLong" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">ข้อตกลงและเงื่อนไข</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>Cras mattis consectetur purus sit amet fermentum. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Morbi leo risus, porta ac consectetur ac, vestibulum at eros
            <p>Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor.</p>
            <p>Aenean lacinia bibendum nulla sed consectetur. Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Donec sed odio dui. Donec ullamcorper nulla non metus auctor fringilla.</p>
            <p>Cras mattis consectetur purus sit amet fermentum. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Morbi leo risus, porta ac consectetur ac, vestibulum at eros.</p>
            <p>Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>